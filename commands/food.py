import discord
from pet import gotPet
from pet import petStats
from player import check_exist_player

def check_exist_food(id):
  #check if the user exist.
  text_file = open("food.txt", "r")
  data = text_file.read()
  text_file.close()
  #if the user exist the update his data.
  if id not in data:
    #if the user not exist then add him to the database.
    with open("food.txt", "a") as f:
      f.write(id + "&\n")
      f.close()

def buyFood(id, food):
  #make sure user in database.
  check_exist_food(id)
  with open("food.txt") as f:
    lines = f.readlines()
    f.close()
  if food.lower() == "apple":
    food = "apple^1"
  elif food.lower() == "banana":
    food = "banana^3"
  elif food.lower() == "fish":
    food = "fish^17"
  elif food.lower() == "chicken":
    food = "chicken^38"
  elif food.lower() == "meat":
    food = "meat^80"
  elif food.lower() == "steak":
    food = "steak^220"
  elif food.lower() == "max-level":
    food = "max level^99500"
  for i in range(len(lines)):
    if id in lines[i]:
      if food + "!" in lines[i]: #means has above 1
        amount = int(lines[i].split("!")[1].split("#")[0])
        currItems = lines[i].split("&")[1].split(food + "!" + str(amount))[0] + food + "!" + str(amount + 1) + lines[i].split("&")[1].split(food + "!" + str(amount))[1]
      elif food in lines[i]: #means has only 1
        currItems = lines[i].split("&")[1].split(food)[0] + food + "!2" + lines[i].split("&")[1].split(food)[1]
      else: #dont have the food
        if len(lines[i].split("&")[1]) > 0:
          currItems = lines[i].split("&")[1] + "#" + food
        else:
          currItems = lines[i].split("&")[1] + food
      currItems = currItems.replace("\n", "")
      lines[i] = lines[i].split("&")[0] + "&" + currItems + "\n"
      data = "".join(lines)
      with open("food.txt", "w") as f:
        f.write(data)
        f.close()

async def food(message, check):
  if message.content.lower()[:4] == "food":
    if message.content.count(" ") == 1:
      msg = message.content.lower().split(" ")
      if len(msg[1]) > 0:
        if msg[1][2:-1].isdigit() == False:
          await message.channel.send("you have to tag a user! (food @user)")
          return 1
        if message.guild.get_member(int(msg[1][2:-1])) is not None:
          check_exist_food(msg[1][2:-1])
          id = msg[1][2:-1]
        else:
          await message.channel.send("this user isn't in the server!")
          return 1
    else:
      check_exist_food(str(message.author.id))
      id = str(message.author.id)
    with open("food.txt") as f:
      lines = f.readlines()
      f.close()
    for i in range(len(lines)):
      if id in lines[i]:
        lines[i] = lines[i].replace("!", " Amount - ")
        lines[i] = lines[i].replace("^", " Exp: ")
        myEmbed = discord.Embed(title=message.guild.get_member(int(id)).name + "'s food Storage!", description=lines[i].split("&")[1].replace("#", "\n"))
        await message.channel.send(embed=myEmbed)
    return 1
  return check

def checkMax_pet(id):
  with open("pet.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      lines[i] = lines[i].replace("!", " - ")
      if int(lines[i].split("#")[1]) >= 200:
        return True
      return False

def checkMax_player(id):
  with open("player.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      lines[i] = lines[i].replace("!", " - ")
      if int(lines[i].split("#")[1]) >= 200:
        return True
      return False

async def feed(message, check):
  if message.content.lower()[:4] == "feed" and ("pet" not in message.content.lower()[5:8] and "me" not in message.content.lower()[5:7]):
    await message.channel.send("you should use feed me <food> / feed pet <food>")
  elif message.content.lower()[:8] == "feed pet":
    #check if user has pet.
    flag = gotPet(str(message.author.id))
    if flag == False:
      #check if pet is max lvl.
      isMax = checkMax_pet(str(message.author.id))
      if isMax == False:
        check_exist_food(str(message.author.id))
        foodName = message.content[9:]
        with open("food.txt") as f:
          lines = f.readlines()
          f.close()
          for i in range(len(lines)):
            if str(message.author.id) in lines[i]:
              if foodName.lower() in lines[i].lower():
                if '!' in lines[i].lower().split(foodName.lower())[1].split("#")[0]:
                  if int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[1]) > 2:
                    if foodName.lower() == "max level":
                      expAmount = 6969
                    else:
                      expAmount = int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("^")[1].split("!")[0])
                    if '#' in lines[i].lower().split(foodName.lower())[1]:
                      lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[0] + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[1]) - 1)) + '#' + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
                    elif '#' in lines[i]: #last item
                      lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("!")[0] + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("!")[1]) - 1))
                    else: #the only item
                      lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("!")[0] + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("!")[1]) - 1))
                  else: #got 2 of this item so remove '!'
                    if foodName.lower() == "max level":
                      expAmount = 6969
                    else:
                      expAmount = int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("^")[1].split("!")[0])
                    if '#' in lines[i].lower().split(foodName.lower())[1]:
                      lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[0] + '#' + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
                    elif '#' in lines[i]:
                      lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("!")[0]
                    else:
                      lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("!")[0]
                else:
                  if '#' in lines[i].lower().split(foodName.lower())[1]:
                    if foodName.lower() == "max level":
                      expAmount = 6969
                    else:
                      expAmount = int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("^")[1])
                    lines[i] = lines[i].lower().split(foodName.lower())[0] + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
                  elif '#' in lines[i]:
                    lines[i] = lines[i].replace("\n", "")
                    if foodName.lower() == "max level":
                      expAmount = 6969
                    else:
                      expAmount = int(lines[i].lower().split(foodName.lower())[1].split("^")[1])
                    lines[i] = lines[i].lower().split('#' + foodName.lower())[0] + "\n"
                  else:
                    lines[i] = lines[i].replace("\n", "")
                    expAmount = int(lines[i].split('^')[1])
                    lines[i] = lines[i].lower().split(foodName.lower())[0] + "\n"
                lines[i] = lines[i].replace("\n", "")
                lines[i] = lines[i] + "\n"
                data = "".join(lines)
                with open("food.txt", "w") as f:
                  f.write(data)
                  f.close()
                await message.channel.send("you feed your pet 1 " + foodName + "!")
                with open("pet.txt") as f:
                  lines = f.readlines()
                  f.close()
                for i in range(len(lines)):
                  if str(message.author.id) in lines[i]:
                    if expAmount == 6969:
                      lines[i] = lines[i].split("#", 1)[0] + '#' + str(200) + '#0#' + lines[i].split('#', 3)[3] + "\n"
                      expAmount = 0
                    else:  
                      expAmount = expAmount + int(lines[i].split("#")[2])
                      while expAmount > int((int(lines[i].split("#")[1]) +  1) * 5):
                        expAmount = expAmount - int((int(lines[i].split("#")[1]) +  1) * 5)
                        lines[i] = lines[i].replace("\n", "")
                        lines[i] = lines[i].split("#", 1)[0] + '#' + str(int(lines[i].split("#")[1]) +  1) + '#' + str(expAmount) + '#' + lines[i].split('#', 3)[3] + "\n"
                    petMsg = lines[i]
                    petInfo = petStats(petMsg.lower().split("&")[1].split("#")[0])
                    originalDamage = float(petInfo.split("#")[4])
                    originalHealth = float(petInfo.split("#")[3])
                    currDamage = float(lines[i].split("#")[4])
                    currHealth = float(lines[i].split("#")[3])
                    petDamage = currDamage + (float(originalDamage) / 100) * float(lines[i].split("#")[1])
                    petHealth = currHealth + (float(originalHealth) / 100) * float(lines[i].split("#")[1])
                    petDamage = round(petDamage, 2)
                    petHealth = round(petHealth, 2)
                    lines[i] = lines[i].replace("\n", "")
                    lines[i] = lines[i].split("#", 1)[0] + '#' + lines[i].split("#")[1] + '#' + str(expAmount) + '#' + str(petHealth) + "#" + str(petDamage) + "\n"
                data = "".join(lines)
                with open("pet.txt", "w") as f:
                  f.write(data)
                  f.close()
                return 1    
          await message.channel.send("You dont have this food!")
          return 1
      else:
        await message.channel.send("your pet is already at max level!") 
    else:
      await message.channel.send("You have to buy a pet to use this command!")
  elif message.content.lower()[:7] == "feed me":
    #check if player exist.
    check_exist_player(str(message.author.id))
    #check if player is max level.
    isMax = checkMax_player(str(message.author.id))
    if isMax == False:
      check_exist_food(str(message.author.id))
      foodName = message.content[8:]
      with open("food.txt") as f:
        lines = f.readlines()
        f.close()
        for i in range(len(lines)):
          if str(message.author.id) in lines[i]:
            if foodName.lower() in lines[i].lower():
              if '!' in lines[i].lower().split(foodName.lower())[1].split("#")[0]:
                if int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[1]) > 2:
                  if foodName.lower() == "max level":
                    expAmount = 6969
                  else:
                    expAmount = int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("^")[1].split("!")[0])
                  if '#' in lines[i].lower().split(foodName.lower())[1]:
                    lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[0] + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[1]) - 1)) + '#' + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
                  elif '#' in lines[i]: #last item
                    lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("!")[0] + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("!")[1]) - 1))
                  else: #the only item
                    lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("!")[0] + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("!")[1]) - 1))
                else: #got 2 of this item so remove '!'
                  if foodName.lower() == "max level":
                    expAmount = 6969
                  else:
                    expAmount = int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("^")[1].split("!")[0])
                  if '#' in lines[i].lower().split(foodName.lower())[1]:
                    lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[0] + '#' + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
                  elif '#' in lines[i]:
                    lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("!")[0]
                  else:
                    lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("!")[0]
              else:
                if '#' in lines[i].lower().split(foodName.lower())[1]:
                  if foodName.lower() == "max level":
                    expAmount = 6969
                  else:
                    expAmount = int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("^")[1])
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
                elif '#' in lines[i]:
                  lines[i] = lines[i].replace("\n", "")
                  if foodName.lower() == "max level":
                    expAmount = 6969
                  else:
                    expAmount = int(lines[i].lower().split(foodName.lower())[1].split("^")[1])
                  lines[i] = lines[i].lower().split('#' + foodName.lower())[0] + "\n"
                else:
                  lines[i] = lines[i].replace("\n", "")
                  expAmount = int(lines[i].split('^')[1])
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + "\n"
              lines[i] = lines[i].replace("\n", "")
              lines[i] = lines[i] + "\n"
              data = "".join(lines)
              with open("food.txt", "w") as f:
                f.write(data)
                f.close()
              await message.channel.send("you ate 1 " + foodName + "!")
              with open("player.txt") as f:
                lines = f.readlines()
                f.close()
              for i in range(len(lines)):
                if str(message.author.id) in lines[i]:
                  if expAmount == 6969:
                    lines[i] = lines[i].split("#", 1)[0] + '#' + str(200) + '#0#' + lines[i].split('#', 3)[3] + "\n"
                    expAmount = 0
                  else:  
                    expAmount = expAmount + int(lines[i].split("#")[2])
                    while expAmount > int((int(lines[i].split("#")[1]) +  1) * 5):
                      expAmount = expAmount - int((int(lines[i].split("#")[1]) +  1) * 5)
                      lines[i] = lines[i].replace("\n", "")
                      lines[i] = lines[i].split("#", 1)[0] + '#' + str(int(lines[i].split("#")[1]) +  1) + '#' + str(expAmount) + '#' + lines[i].split('#', 3)[3] + "\n"
                  originalDamage = float(10)
                  originalHealth = float(100)
                  currDamage = float(lines[i].split("#")[4])
                  currHealth = float(lines[i].split("#")[3])
                  petDamage = currDamage + (float(originalDamage) / 10) * float(lines[i].split("#")[1])
                  petHealth = currHealth + (float(originalHealth) / 10) * float(lines[i].split("#")[1])
                  petDamage = round(petDamage, 2)
                  petHealth = round(petHealth, 2)
                  lines[i] = lines[i].replace("\n", "")
                  lines[i] = lines[i].split("#", 1)[0] + '#' + lines[i].split("#")[1] + '#' + str(expAmount) + '#' + str(petHealth) + "#" + str(petDamage) + "\n"
              data = "".join(lines)
              with open("player.txt", "w") as f:
                f.write(data)
                f.close()
              return 1    
        await message.channel.send("You dont have this food!")
        return 1
    else:
      await message.channel.send("youre already at max level!")
      return 1
  else:
    return check