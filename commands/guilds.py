import discord
from player import check_exist_player

def gotTicket(id):
  with open("inventory.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      if "guildticket" in lines[i].lower():
        return True
      return False

def useGuildTicket(id, item):
    foodName = item
    with open("inventory.txt") as f:
      lines = f.readlines()
      f.close()
      for i in range(len(lines)):
        if id in lines[i]:
          if foodName.lower() in lines[i].lower():
            if '!' in lines[i].lower().split(foodName.lower())[1].split("#")[0]:
              if int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[1]) > 2:
                if '#' in lines[i].lower().split(foodName.lower())[1]:
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[0] + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[1]) - 1)) + '#' + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
                elif '#' in lines[i]:
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("!")[1]) - 1))
                else:
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("!")[1]) - 1))
              else: #got 2 of this item so remove '!'
                if '#' in lines[i].lower().split(foodName.lower())[1]:
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + '#' + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
                elif '#' in lines[i]: #above 2 items in inventory
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower()
                else: #only this item in inventory
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower()
            else:
              if '#' in lines[i].lower().split(foodName.lower())[1]:
                lines[i] = lines[i].lower().split(foodName.lower())[0] + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
              elif '#' in lines[i]:
                lines[i] = lines[i].replace("\n", "")
                lines[i] = lines[i].lower().split('#' + foodName.lower())[0] + "\n"
              else:
                lines[i] = lines[i].replace("\n", "")
                lines[i] = lines[i].lower().split(foodName.lower())[0] + "\n"
            lines[i] = lines[i].replace("\n", "")
            lines[i] = lines[i] + "\n"
            data = "".join(lines)
            with open("inventory.txt", "w") as f:
              f.write(data)
              f.close()

def alreadyExist(name):
  with open("guilds.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if name.lower() in lines[i].lower():
      return True
  return False

#this function return the player game name.
def getPlayerName(id):
  with open("player.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      return lines[i].split("&")[1].split("#")[0]

#this function add guild to the database and the playername as leader
def addGuild(name, player):
  #remember to mark leader with * and when print guild replace * with (Leader)
   with open("guilds.txt", "a") as f:
    f.write(name + "&" + player + "*\n")
    f.close()

def hasChangedName(id):
  with open("player.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i].lower():
      if lines[i].lower().split("&")[1].split("#")[0] == "new player":
        return False
      return True

async def createGuild(message, check):
  if message.content.lower()[:11] == "createguild":
    #check if player already changed his name.
    check_exist_player(str(message.author.id))
    flag3 = hasChangedName(str(message.author.id))
    if flag3 == True:
      #check if guild with that name already exist.
      flag1 = alreadyExist(message.content.lower()[12:])
      if flag1 == False:
        #check if guild name is not above 20 letters.
        if len(message.content.lower()[12:]) <= 20:
          flag2 = gotTicket(str(message.author.id))
          if flag2 == True:
            if hasGuild(getPlayerName(str(message.author.id))):
              await message.channel.send("You are already in a guild!")
              return 1
            else:
              useGuildTicket(str(message.author.id), "guildticket")
              playerName = getPlayerName(str(message.author.id))
              addGuild(message.content[12:], playerName)
              await message.channel.send("The guild " + message.content.lower()[12:] + " has beed created by " + playerName + "!")
              return 1
          else:
            await message.channel.send("You first have to buy guildTicket from the shop!")
            return 1
        else:
          await message.channel.send("The guild name can be at most 20 chars!")
          return 1
      else:
        await message.channel.send("There is already guild with that name!")
        return 1
    else:
      await message.channel.send("You have to change your name first!")
      return 1
  return check

def getPlayerLevel(name):
  with open("player.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    name = name.replace("\n", "")
    if name.lower() in lines[i].lower():
      return lines[i].split("#")[1]
  return "fail"


def hasGuild(name):
  with open("guilds.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if name.lower() in lines[i].lower():
      return True
  return False

def getGuild(name):
  with open("guilds.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if name.lower() in lines[i].lower():
      return lines[i].split("&")[0]
    
async def guilds(message, check):
  if message.content.lower()[:5] == "guild": 
    if message.content.lower() == "guild":
      playerName = getPlayerName(str(message.author.id))
      if playerName.lower() == "new player":
        await message.channel.send("Your'e not in guild! (you should change your name from default tho, use changeName <name>)")
        return 1
      flagGuild = hasGuild(playerName)
      if flagGuild == False:
        await message.channel.send("Your'e not in guild!")
        return 1
      playerGuild = getGuild(playerName)
      message.content = message.content + " " + playerGuild
    if len(message.content.lower()[6:]) > 0:
      flag1 = alreadyExist(message.content.lower()[6:])
      if flag1 == True:
        #embed of the guild, remember replace * with (leader)
        with open("guilds.txt") as f:
          lines = f.readlines()
          f.close()
        for i in range(len(lines)):
          if message.content.lower()[6:] in lines[i].lower():
            myEmbed = discord.Embed(title=lines[i].split("&")[0] + " Guild")
            lines[i] = lines[i].replace("*", " (Leader)")
            guildMembers = lines[i].split("&")[1].split("#")
            for i in range(len(guildMembers)):
              if len(guildMembers) > 0: #means has members
                if " (Leader)" in guildMembers[i]:
                  memberLevel = getPlayerLevel(guildMembers[i].split(" (Leader)")[0])
                else:
                  memberLevel = getPlayerLevel(guildMembers[i])
              myEmbed.add_field(name=guildMembers[i], value="Level: " + memberLevel, inline=False)
            await message.channel.send(embed=myEmbed)
            return 1
      else:
        await message.channel.send("There is not guild with that name!")
        return 1
    else:
      await message.channel.send("you should type also the guild name! (guild <name>)")
      return 1
  return check

def inGuild(name):
  with open("guilds.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if name in lines[i]:
      return True
  return False
  
#send join request to enter clan
async def join(message, check):
  if message.content.lower()[:4] == "join":
    if len(message.content.lower()[5:]) > 0:
      check_exist_player(str(message.author.id))
      flag1 = alreadyExist(message.content.lower()[5:]) 
      if flag1 == True:
        playerName = getPlayerName(str(message.author.id))
        if playerName.lower() == "new player":
          await message.channel.send("You have to change your name first!")
          return 1
        else:
          flag2 = inGuild(playerName)
          if flag2 == False:
            #check if the player already sent request to this guild
            with open("requests.txt") as f:
              lines = f.readlines()
              f.close()
            for i in range(len(lines)):
              if message.content.lower()[5:] in lines[i].lower() and playerName.lower() in lines[i].lower():
                await message.channel.send("You already sent join request to this guild!")
                return 1
            with open("requests.txt", "a") as f:
              f.write(message.content[5:] + "&" + playerName + "\n")
              f.close()
            await message.channel.send("Join request to " + message.content.lower()[5:] + " sent successfuly!")
            return 1
          else:
            await message.channel.send("Your'e already in a guild!")
            return 1
      else:
        await message.channel.send("There is not guild with that name!")
        return 1
    else:
      await message.channel.send("you should type also the guild name! (join <GuildName>)")
      return 1
  return check

def isGuildLeader(guild, player):
  with open("guilds.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if guild in lines[i].lower():
      if player.lower() + "*" in lines[i].lower():
        return True
      return False

def addMember(guild, member):
  with open("guilds.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if guild.lower() in lines[i].lower():
      lines[i] = lines[i].replace("\n", "")
      lines[i] = lines[i] + "#" + member + "\n"
      data = "".join(lines)
      with open("guilds.txt", "w") as f:
        f.write(data)
        f.close()

def removeRequests(player):
  with open("requests.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if player in lines[i]:
      lines.remove(lines[i])
  data = "".join(lines)
  with open("requests.txt", "w") as f:
    f.write(data)
    f.close()
      

async def accept(message, check):
  if message.content.lower()[:6] == "accept":
    if len(message.content.lower()[7:]) > 0:
      check_exist_player(str(message.author.id))
      authorPlayerName = getPlayerName(str(message.author.id))
      with open("requests.txt") as f:
        lines = f.readlines()
        f.close()
      for i in range(len(lines)):
        if message.content.lower()[7:] in lines[i].lower():
          requestedPlayer = lines[i].lower().split("&")[1].replace("\n", "")
          requestedGuild = lines[i].lower().split("&")[0]
          flag1 = isGuildLeader(requestedGuild, authorPlayerName)
          if flag1 == True:
            #remove all join requests from this player
            removeRequests(requestedPlayer)
            addMember(requestedGuild, requestedPlayer)
            await message.channel.send("The player " + requestedPlayer + " has joined the guild " + requestedGuild + "!")
            return 1
          else:
            await message.channel.send("Only the guild leader can accept new members!")
            return 1
      await message.channel.send("this player didn't requested to join your guild!")
    else:
      await message.channel.send("you should type also the player name! (accept <PlayerName>)")
      return 1
  return check


def removeMember(guild, member):
  with open("guilds.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if guild.lower() in lines[i].lower():
      lines[i] = lines[i].replace("\n", "")
      lines[i] = lines[i].replace("#" + member, "")
      data = "".join(lines)
      with open("guilds.txt", "w") as f:
        f.write(data)
        f.close()

async def kick(message, check):
  if message.content.lower()[:4] == "kick":
    if len(message.content.lower()[5:]) > 0:
      check_exist_player(str(message.author.id))
      authorPlayerName = getPlayerName(str(message.author.id))
      with open("guilds.txt") as f:
        lines = f.readlines()
        f.close()
      for i in range(len(lines)):
        if message.content.lower()[5:] in lines[i].lower():
          requestedPlayer = message.content.lower()[5:]
          requestedGuild = lines[i].lower().split("&")[0]
          flag1 = isGuildLeader(requestedGuild, authorPlayerName)
          if flag1 == True:
            removeMember(requestedGuild, requestedPlayer)
            await message.channel.send("The player " + requestedPlayer + " kicked our from the guild " + requestedGuild + "!")
            return 1
          else:
            await message.channel.send("Only the guild leader can kick members!")
            return 1
      await message.channel.send("this player isn't in your guild!")
    else:
      await message.channel.send("you should type also the player name! (kick <PlayerName>)")
      return 1
  return check
