import discord #use it for embed msg when display player and dealer cards, do dict with 51 cards key = card name val = card value, then in the embedd print for user cards got: (the cards keys names) and total value (the val of all the keys together) the same for the dealer.
from userData import check_exist
from userData import userMoney
from userData import removeMoney
from userData import addMoney
import random

cards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]

def alreadyExist(id):
  with open("blackjack.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if str(id) in lines[i]:
      return True
  return False

def CheckCard(index, above):
  if(index == 0 or index == 13 or index == 26 or index == 39):
    if above == False:
      return str(10) + " - Ace"
    else:
      return str(cards[index]) + " - Ace"
  if(index == 10 or index == 23 or index == 36 or index == 49):
    return str(cards[index]) + " - Jack"
  if(index == 11 or index == 24 or index == 37 or index == 50):
    return str(cards[index]) + " - Queen"
  if(index == 12 or index == 25 or index == 38 or index == 51):
    return str(cards[index]) + " - King"
  return str(cards[index])

async def blackJack(message, check):
  if message.content.lower().split(" ")[0] == "bj" or message.content.lower().split(" ")[0] == "blackjack":
    if message.content.count(" ") == 1:
      if message.content.split(" ")[1].isdigit():
        check_exist(message)
        money = await userMoney(message)
        if int(money) >= int(message.content.split(" ")[1]):
          if alreadyExist(message.author.id) == False:
            #get 2 random cards for the player.
            n = random.randint(0, 51)
            pCard1 = n
            while n == pCard1:
              n = random.randint(0, 51)
            pCard2 = n
            #get 2 random cards for the dealer.
            while n == pCard1 or n == pCard2:
              n = random.randint(0, 51)
            dCard1 = n
            while n == pCard1 or n == pCard2 or n == dCard1:
              n = random.randint(0, 51)
            dCard2 = n
            with open("blackjack.txt", "a") as f:
              f.write(str(message.author.id) + "&" + message.content.split(" ")[1] + "$" + str(pCard1) + "#" + str(pCard2) + "*" + str(dCard1) + "#" + str(dCard2) + "\n")
              f.close()
            myEmbed = discord.Embed(title=message.author.name + "'s BlackJack:")
            myEmbed.add_field(name="My Cards:", value="Card 1 = " + CheckCard(pCard1, False) + "\nCard2 = " + CheckCard(pCard2, False) + "\nTotal Score: " + str(int(CheckCard(pCard1, False).split(" ")[0]) + int(CheckCard(pCard2, False).split(" ")[0])), inline=False)
            myEmbed.add_field(name="Dealer Cards:", value="Card 1 = " + CheckCard(dCard1, True) + "\nCard2 =  Hidden\nTotal Score: " + str(int(CheckCard(dCard1, False).split(" ")[0])), inline=False)
            myEmbed.add_field(name="Type hit or stand to continue!", value="Bet Value = " + message.content.split(" ")[1], inline=False)
            await message.channel.send(embed=myEmbed)
            return 1 #good arguments now start the game.
          else:
            await message.channel.send("You already have a bj game running, pls type hit or stand to continue...")
            return 1
        else:
          await message.channel.send("You dont have enough money...")
          return 1
      else:
        await message.channel.send("use money bozo u cant trick me...")
    elif message.content.count(" ") > 1:
      await message.channel.send("too many arguments! please use bj [amount]")
      return 1
    else:
        await message.channel.send("You have to bet some of money!")
        return 1
    return 1
  return check

def AceAmount(Mycards):
  count = 0
  for card in range(len(Mycards)):
    if "Ace" in CheckCard(int(Mycards[card]), False):
      count += 1
  return count

async def lose(message, pCards, bust):
  #if bust == true that means he got over 21 so dont need to check dealer results.
  strR = ""
  ts = 0
  with open("blackjack.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if str(message.author.id) in lines[i]:
      money = lines[i]
      lines[i] = ""
  money = int(money.split("&")[1].split("$")[0])
  for i in range(len(pCards)):
    strR += "Card " + str(i) + " = " + pCards[i] + "\n"
    ts += int(pCards[i].split(" ")[0])
  myEmbed = discord.Embed(title= message.author.name + "'s BlackJack: BUST")
  myEmbed.add_field(name="My Cards:", value=strR + "\nTotal Score: " + str(ts), inline=False)
  myEmbed.add_field(name="You lost: ", value=str(money) + "$", inline=True)
  data = "".join(lines)
  with open("blackjack.txt", "w") as f:
    f.write(data)
    f.close()
  await removeMoney(str(message.author.id), money)
  await message.channel.send(embed=myEmbed)

async def win(message, pCards, bj):
  #get dealer results, and check if won, if bj == true then reward x1.5.
  #get dealer completed cards.
  dCards = []
  aLine = ""
  with open("blackjack.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if str(message.author.id) in lines[i]:
      money = lines[i]
      aLine = lines[i]
      lines[i] = ""
  money = int(money.split("&")[1].split("$")[0])
  strR = ""
  ts = 0
  for i in range(len(pCards)):
    strR += "Card " + str(i) + " = " + pCards[i] + "\n"
    ts += int(pCards[i].split(" ")[0])
  totalValueD = 0
  for i in range(len(aLine.split("*")[1].split("#"))):
    if "Ace" not in CheckCard(int(aLine.split("*")[1].split("#")[i]), False):
      totalValueD += cards[int(aLine.split("*")[1].split("#")[i])]
  sum = AceAmount(aLine.split("*")[1].split("#"))
  while totalValueD + sum < 21:
    totalValueD = 0
    for i in range(len(aLine.split("*")[1].split("#"))):
      if "Ace" not in CheckCard(int(aLine.split("*")[1].split("#")[i]), False):
        totalValueD += cards[int(aLine.split("*")[1].split("#")[i])]
    n = random.randint(0, 51)
    for i in range(len(lines)):
      if str(message.author.id) in lines[i]:
        while str(n) in aLine:
          n = random.randint(0, 51)
    aLine = aLine.split("*")[0] + "*" + aLine.split("*")[1] + "#" + str(n)
    if "Ace" not in CheckCard(n, True):
      totalValueD += cards[n]
    AboveAce = 0
    sum = AceAmount(aLine.split("*")[1].split("#"))
    stop = False
    while sum > 0 and stop == False:
      if totalValueD + sum - 1 <= 11:
        totalValueD += 10
        AboveAce += 1
        sum -= 1
      else:
        stop = True
  for i in range(len(aLine.split("*")[1].split("#"))):
    if AboveAce > 0:
      t = CheckCard(int(aLine.split("*")[1].split("#")[i]), False)
    else:
      t = CheckCard(int(aLine.split("*")[1].split("#")[i]), True)
    if "Ace" in t:
      if AboveAce > 0:
        AboveAce -= 1
      else:
        sum -= 1
    dCards.append(t)
  tsd = 0
  strRD = ""
  for i in range(len(dCards)):
    strRD += "Card " + str(i + 1) + " = " + dCards[i] + "\n"
    tsd += int(dCards[i].split(" ")[0])
  #
  if tsd > ts:
    #win, dealer above 21
    strRD = ""
    tsd = 0
    for i in range(len(dCards)):
      strRD += "Card " + str(i + 1) + " = " + dCards[i] + "\n"
      tsd += int(dCards[i].split(" ")[0])
    myEmbed = discord.Embed(title= message.author.name + "'s BlackJack: WIN BJ!")
    myEmbed.add_field(name="My Cards:", value=strR + "\nTotal Score: " + str(ts), inline=False)
    myEmbed.add_field(name="Dealer Cards:", value=strRD + "\nTotal Score: " + str(tsd), inline=False)
    myEmbed.add_field(name="BlackJack!(x1.5 prize) You won: ", value=str(money * 1.5) + "$", inline=True)
    data = "".join(lines)
    with open("blackjack.txt", "w") as f:
      f.write(data)
      f.close()
    await addMoney(str(message.author.id), money)
    await message.channel.send(embed=myEmbed)
  else:
    #PUSH! (draw)
    strRD = ""
    tsd = 0
    for i in range(len(dCards)):
      strRD += "Card " + str(i + 1) + " = " + dCards[i] + "\n"
      tsd += int(dCards[i].split(" ")[0])
    myEmbed = discord.Embed(title= message.author.name + "'s BlackJack: Push!")
    myEmbed.add_field(name="My Cards:", value=strR + "\nTotal Score: " + str(ts), inline=False)
    myEmbed.add_field(name="Dealer Cards:", value=strRD + "\nTotal Score: " + str(tsd), inline=False)
    myEmbed.add_field(name="Push!", value="you didn't won any money since it's draw!", inline=False)
    data = "".join(lines)
    with open("blackjack.txt", "w") as f:
      f.write(data)
      f.close()
    await message.channel.send(embed=myEmbed)

async def currResults(message, pCards, dCard1):
  strR = ""
  ts = 0
  with open("blackjack.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if str(message.author.id) in lines[i]:
      money = lines[i]
      lines[i] = ""
  money = int(money.split("&")[1].split("$")[0])
  for i in range(len(pCards)):
    strR += "Card " + str(i) + " = " + pCards[i] + "\n"
    ts += int(pCards[i].split(" ")[0])
  myEmbed = discord.Embed(title= message.author.name + "'s BlackJack:")
  myEmbed.add_field(name="My Cards:", value=strR + "\nTotal Score: " + str(ts), inline=False)
  myEmbed.add_field(name="Dealer Cards:", value="Card 1 = " + CheckCard(int(dCard1), True) + "\nCard2 =  Hidden\nTotal Score: " + str(int(CheckCard(int(dCard1), False).split(" ")[0])), inline=False)
  myEmbed.add_field(name="Type hit or stand to continue!", value="Bet Value = " + str(money), inline=False)
  await message.channel.send(embed=myEmbed)

async def Hit(message, check):
  if message.content.lower().split(" ")[0] == "hit":
    if alreadyExist(message.author.id) == False:
      await message.channel.send("You have to start a bj game first, please use bj <amount>")
      return 1
    else:
      pCards = []
      n = random.randint(0, 51)
      aLine = ""
      with open("blackjack.txt") as f:
        lines = f.readlines()
        f.close()
      b = 0
      for i in range(len(lines)):
        if str(message.author.id) in lines[i]:
          aLine = lines[i]
          b = i
          while str(n) in lines[i]:
            n = random.randint(0, 51)
      aLine = aLine.split("$")[0] + "$" + aLine.split("$")[1].split("*")[0] + "#" + str(n) + "*" + aLine.split("$")[1].split("*")[1]
      lines[b] = aLine
      data = "".join(lines)
      with open("blackjack.txt", "w") as f:
        f.write(data)
        f.close()
      totalValue = 0
      totalAce = AceAmount(aLine.split("$")[1].split("*")[0].split("#"))
      for i in range(len(aLine.split("$")[1].split("*")[0].split("#"))):
        totalValue += cards[int(aLine.split("$")[1].split("*")[0].split("#")[i])]
      if totalAce == 3 and len(aLine.split("$")[1].split("*")[0].split("#")) == 3:
        #win (bj), 2 Aces = 10, 1 Ace = 1.
        pCards.append(CheckCard(0, False))
        pCards.append(CheckCard(0, False))
        pCards.append(CheckCard(0, True))
        await win(message, pCards, True)
        return 1
      elif totalAce == 0 and totalValue > 21:
        #lose, 0 Aces.
        for i in range(len(aLine.split("$")[1].split("*")[0].split("#"))):
          pCards.append(CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), False))
        await lose(message, pCards, True) #bust.
        return 1
      elif totalValue == 21:
        #win (bj), if were Ace then they worth 1.
        for i in range(len(aLine.split("$")[1].split("*")[0].split("#"))):
          pCards.append(CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), True))
        await win(message, pCards, True)
        return 1
      else:
        totalValue = 0
        sum = 0
        for i in range(len(aLine.split("$")[1].split("*")[0].split("#"))):
          if "Ace" in CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), False):
            sum += 1
          else:
            totalValue += cards[int(aLine.split("$")[1].split("*")[0].split("#")[i])]
        if totalValue > 21:
          #lose, Above 21 no matter the result of the Aces, put it as 1 if was.
          for i in range(len(aLine.split("$")[1].split("*")[0].split("#"))):
            pCards.append(CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), True))
          await lose(message, pCards, True) #bust.
          return 1
        else:
          #score under 21.
          AboveAce = 0
          stop = False
          while sum > 0 and stop == False:
            if totalValue + sum - 1 <= 11:
              totalValue += 10
              AboveAce += 1
              sum -= 1
            else:
              stop = True
          if totalValue + sum > 21:
            #lose
            used = []
            #print current result, not win / lose. AboveAce - amount of Aces above, sum = amount of not above, totalValue = the rest.
            for i in range(len(aLine.split("$")[1].split("*")[0].split("#"))):
              if AboveAce > 0:
                t = CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), False)
              else:
                t = CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), True)
              if "Ace" in t:
                if AboveAce > 0:
                  AboveAce -= 1
                else:
                  sum -= 1
              pCards.append(t)
            await lose(message, pCards, True)
            return 1
          if totalValue + sum == 21:
            #win, AboveAce - amount of Aces above, sum = amount of not above, totalValue = the rest.
            used = []
            #print current result, not win / lose. AboveAce - amount of Aces above, sum = amount of not above, totalValue = the rest.
            for i in range(len(aLine.split("$")[1].split("*")[0].split("#"))):
              if AboveAce > 0:
                t = CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), False)
              else:
                t = CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), True)
              if "Ace" in t:
                if AboveAce > 0:
                  AboveAce -= 1
                else:
                  sum -= 1
              pCards.append(t)
            await win(message, pCards, True)
            return 1
          else:
            used = []
            #print current result, not win / lose. AboveAce - amount of Aces above, sum = amount of not above, totalValue = the rest.
            for i in range(len(aLine.split("$")[1].split("*")[0].split("#"))):
              if AboveAce > 0:
                t = CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), False)
              else:
                t = CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), True)
              if "Ace" in t:
                if AboveAce > 0:
                  AboveAce -= 1
                else:
                  sum -= 1
              pCards.append(t)
            await currResults(message, pCards, aLine.split("$")[1].split("*")[1].split("#")[0])
            return 1
      return 1
    return 1
  return check


async def stand(message, check):
  if message.content.lower().split(" ")[0] == "stand":
    if alreadyExist(message.author.id) == False:
      await message.channel.send("You have to start a bj game first, please use bj <amount>")
      return 1
    else:
      pCards = []
      dCards = []
      aLine = ""
      with open("blackjack.txt") as f:
        lines = f.readlines()
        f.close()
      for i in range(len(lines)):
        if str(message.author.id) in lines[i]:
          aLine = lines[i].replace("\n", "")
      #get player completed cards.
      sum = AceAmount(aLine.split("$")[1].split("*")[0].split("#"))
      totalValue = 0
      for i in range(len(aLine.split("$")[1].split("*")[0].split("#"))):
          if "Ace" not in CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), False):
            totalValue += cards[int(aLine.split("$")[1].split("*")[0].split("#")[i])]
      AboveAce = 0
      stop = False
      while sum > 0 and stop == False:
        if totalValue + sum - 1 <= 11:
          totalValue += 10
          AboveAce += 1
          sum -= 1
        else:
          stop = True
      for i in range(len(aLine.split("$")[1].split("*")[0].split("#"))):
        if AboveAce > 0:
          t = CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), False)
        else:
          t = CheckCard(int(aLine.split("$")[1].split("*")[0].split("#")[i]), True)
        if "Ace" in t:
          if AboveAce > 0:
            AboveAce -= 1
          else:
            sum -= 1
        pCards.append(t)
      #get dealer completed cards.
      totalValueD = 0
      for i in range(len(aLine.split("*")[1].split("#"))):
        if "Ace" not in CheckCard(int(aLine.split("*")[1].split("#")[i]), False):
          totalValueD += cards[int(aLine.split("*")[1].split("#")[i])]
      while totalValueD < totalValue and totalValueD < 21:
        totalValueD = 0
        for i in range(len(aLine.split("*")[1].split("#"))):
          if "Ace" not in CheckCard(int(aLine.split("*")[1].split("#")[i]), False):
            totalValueD += cards[int(aLine.split("*")[1].split("#")[i])]
        n = random.randint(0, 51)
        for i in range(len(lines)):
          if str(message.author.id) in lines[i]:
            while str(n) in aLine:
              n = random.randint(0, 51)
        aLine = aLine.split("*")[0] + "*" + aLine.split("*")[1] + "#" + str(n)
        if "Ace" not in CheckCard(n, True):
          totalValueD += cards[n]
        sum = AceAmount(aLine.split("*")[1].split("#"))
        AboveAce = 0
        stop = False
        while sum > 0 and stop == False:
          if totalValueD + sum - 1 <= 11:
            totalValueD += 10
            AboveAce += 1
            sum -= 1
          else:
            stop = True
      for i in range(len(aLine.split("*")[1].split("#"))):
        if AboveAce > 0:
          t = CheckCard(int(aLine.split("*")[1].split("#")[i]), False)
        else:
          t = CheckCard(int(aLine.split("*")[1].split("#")[i]), True)
        if "Ace" in t:
          if AboveAce > 0:
            AboveAce -= 1
          else:
            sum -= 1
        dCards.append(t)
      if totalValueD + sum > totalValue and totalValueD + sum <= 21:
        #lose.
        strR = ""
        strRD = ""
        ts = 0
        tsd = 0
        with open("blackjack.txt") as f:
          lines = f.readlines()
          f.close()
        for i in range(len(lines)):
          if str(message.author.id) in lines[i]:
            money = lines[i]
            lines[i] = ""
        money = int(money.split("&")[1].split("$")[0])
        for i in range(len(pCards)):
          strR += "Card " + str(i) + " = " + pCards[i] + "\n"
          ts += int(pCards[i].split(" ")[0])
        for i in range(len(dCards)):
          strRD += "Card " + str(i) + " = " + dCards[i] + "\n"
          tsd += int(dCards[i].split(" ")[0])
        myEmbed = discord.Embed(title= message.author.name + "'s BlackJack: LOSE")
        myEmbed.add_field(name="My Cards:", value=strR + "\nTotal Score: " + str(ts), inline=False)
        myEmbed.add_field(name="Dealer Cards:", value=strRD + "\nTotal Score: " + str(tsd), inline=False)
        myEmbed.add_field(name="You lost: ", value=str(money) + "$", inline=True)
        data = "".join(lines)
        with open("blackjack.txt", "w") as f:
          f.write(data)
          f.close()
        await removeMoney(str(message.author.id), money)
        await message.channel.send(embed=myEmbed)
        return 1
      else:
        #won
        strR = ""
        strRD = ""
        ts = 0
        tsd = 0
        with open("blackjack.txt") as f:
          lines = f.readlines()
          f.close()
        for i in range(len(lines)):
          if str(message.author.id) in lines[i]:
            money = lines[i]
            lines[i] = ""
        money = int(money.split("&")[1].split("$")[0])
        for i in range(len(pCards)):
          strR += "Card " + str(i + 1) + " = " + pCards[i] + "\n"
          ts += int(pCards[i].split(" ")[0])
        for i in range(len(dCards)):
          strRD += "Card " + str(i + 1) + " = " + dCards[i] + "\n"
          tsd += int(dCards[i].split(" ")[0])
        myEmbed = discord.Embed(title= message.author.name + "'s BlackJack: WIN")
        myEmbed.add_field(name="My Cards:", value=strR + "\nTotal Score: " + str(ts), inline=False)
        myEmbed.add_field(name="Dealer Cards:", value=strRD + "\nTotal Score: " + str(tsd), inline=False)
        myEmbed.add_field(name="You won: ", value=str(money) + "$", inline=True)
        data = "".join(lines)
        with open("blackjack.txt", "w") as f:
          f.write(data)
          f.close()
        await addMoney(str(message.author.id), money)
        await message.channel.send(embed=myEmbed)
        return 1
  return check