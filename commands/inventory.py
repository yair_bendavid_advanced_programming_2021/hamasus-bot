from userData import hasMoney
from userData import removeMoney
from pet import gotPet
from food import buyFood
from items import buyItem
import discord

def check_exist(id):
  #check if the user exist.
  text_file = open("inventory.txt", "r")
  data = text_file.read()
  text_file.close()
  #if the user exist the update his data.
  if id not in data:
    #if the user not exist then add him to the database.
    with open("inventory.txt", "a") as f:
      f.write(id + "&\n")
      f.close()

"""

"""
def addItem(id, item):
  #make sure user in database.
  check_exist(id)
  with open("inventory.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      if item + "!" in lines[i]: #means has above 1
        amount = int(lines[i].split("!")[1].split("#")[0])
        currItems = lines[i].split("&")[1].split(item + "!" + str(amount))[0] + item + "!" + str(amount + 1) + lines[i].split("&")[1].split(item + "!" + str(amount))[1]
      elif item in lines[i]: #means has only 1
        currItems = lines[i].split("&")[1].split(item)[0] + item + "!2" + lines[i].split("&")[1].split(item)[1]
      else: #dont have the item
        if len(lines[i].split("&")[1]) > 0:
          currItems = lines[i].split("&")[1] + "#" + item
        else:
          currItems = lines[i].split("&")[1] + item
      currItems = currItems.replace("\n", "")
      lines[i] = lines[i].split("&")[0] + "&" + currItems + "\n"
      data = "".join(lines)
      with open("inventory.txt", "w") as f:
        f.write(data)
        f.close()

def buyPet(message, pet):
  with open("pet.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if str(message.author.id) in lines[i]:
      lines[i] = lines[i].replace("\n", "")
      if pet.lower() == "bee":
        lines[i] = lines[i] + "Bee#0#0#10#5\n"
      elif pet.lower() == "rabbit":
        lines[i] = lines[i] + "Rabbit#0#0#20#10\n"
      elif pet.lower() == "frog":
        lines[i] = lines[i] + "Frog#0#0#30#15\n"
      elif pet.lower() == "dog":
        lines[i] = lines[i] + "Dog#0#0#40#20\n"
      elif pet.lower() == "cat":
        lines[i] = lines[i] + "Cat#0#0#40#20\n"
      elif pet.lower() == "fox":
        lines[i] = lines[i] + "Fox#0#0#70#35\n"
      elif pet.lower() == "wolf":
        lines[i] = lines[i] + "Wolf#0#0#100#50\n"
      elif pet.lower() == "bear":
        lines[i] = lines[i] + "Bear#0#0#150#75\n"
      elif pet.lower() == "tiger":
        lines[i] = lines[i] + "Tiger#0#0#200#100\n"
      elif pet.lower() == "lion":
        lines[i] = lines[i] + "Rabbit#0#0#500#250\n"
      elif pet.lower() == "shark":
        lines[i] = lines[i] + "shark#0#0#1000#500\n"
      elif pet.lower() == "dragon":
        lines[i] = lines[i] + "Rabbit#0#0#1500#750\n"
      elif pet.lower() == "demon":
        lines[i] = lines[i] + "Rabbit#0#0#2500#1250\n"
      elif pet.lower() == "demon lord":
        lines[i] = lines[i] + "Demon Lord#0#0#20000#10000\n"
  data = "".join(lines)
  with open("pet.txt", "w") as f:
    f.write(data)
    f.close()

async def buy(message, check):
  msg = message.content.lower().split(" ")
  if msg[0] == "buy":
    flag = False
    hasPet = False
    check_exist(str(message.author.id))
    text_file = open("shop.txt", "r")
    data = text_file.read()
    lines = data.split("\n")
    text_file.close()
    for line in lines:
      if '-' in line:
        line = line.replace("-", " ")
      if message.content.lower()[4:] == line.split("$")[0].lower():
        flag = True
        if hasMoney(str(message.author.id), int(line.split("$")[1])):
          if message.content.lower()[4:].replace(" ", "-") in data.split("#")[2].lower():
            hasPet = gotPet(str(message.author.id))
            if hasPet == False:
              await message.channel.send("You already own a pet!")
              return
            else:
              buyPet(message, line.split("$")[0].lower())
          elif message.content.lower()[4:].replace(" ", "-") in data.split("#")[4].lower():
            buyFood(str(message.author.id), line.split("$")[0].lower())
          elif message.content.lower()[4:].replace(" ", "-") not in data.split("#")[5].lower() and message.content.lower()[4:].replace(" ", "-") not in data.split("#")[6].lower():
              buyItem(str(message.author.id), line.split("$")[0])
          else:
            addItem(str(message.author.id), line.split("$")[0])
          await removeMoney(str(message.author.id), int(line.split("$")[1]))
          await message.channel.send(message.author.name + " bought " + line.split("$")[0] + "!")
        else:
          await message.channel.send("You dont have enough money to buy this item")
    if flag == False:
      await message.channel.send("item not in shop.")
    return 1
  return check

async def inventory(message, check):
  if message.content.lower()[:9] == "inventory":
    if message.content.count(" ") == 1:
      msg = message.content.lower().split(" ")
      if len(msg[1]) > 0:
        if msg[1][2:-1].isdigit() == False:
          await message.channel.send("you have to tag a user! (inventory @user)")
          return 1
        if message.guild.get_member(int(msg[1][2:-1])) is not None:
          check_exist(msg[1][2:-1])
          id = msg[1][2:-1]
        else:
          await message.channel.send("this user isn't in the server!")
          return 1
    else:
      check_exist(str(message.author.id))
      id = str(message.author.id)
    with open("inventory.txt") as f:
      lines = f.readlines()
      f.close()
    for i in range(len(lines)):
      if id in lines[i]:
        lines[i] = lines[i].replace("!", " - ")
        myEmbed = discord.Embed(title=message.guild.get_member(int(id)).name + "'s inventory:", description=lines[i].split("&")[1].replace("#", "\n"))
        await message.channel.send(embed=myEmbed)
        return 1
  return check