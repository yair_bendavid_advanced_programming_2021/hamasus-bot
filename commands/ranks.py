import discord

async def getName(id, client):
  id = str(id)
  user = await client.fetch_user(id)
  return user.name

async def top(message, check, client):
  if message.content.lower()[:3] == "top":
    if message.content.count(" ") == 1: #second arg for top type.
      if message.content.split(" ")[1].lower() == "money":
        with open("database.txt") as f:
          lines = f.readlines()
          f.close()
        kaki = []
        for line in lines:
          if len(line) > 1:
            kaki.append(tuple((line.split("{")[0], int(line.split(":")[2].split(",")[0]))))
        tahat = sorted(kaki, key=lambda tahat: tahat[1])
        tahat.reverse()
        topMoney = ""
        i = 1
        for line in tahat:
          if i > 15:
            break
          topMoney += "Rank " + str(i) + " - " + await getName(line[0], client) + " (" + str(line[1]) + "$)\n"
          i += 1
        topMoney = topMoney[:-1]
        myEmbed = discord.Embed(title="Top 15 Money:", description=topMoney)
      elif message.content.split(" ")[1].lower() == "damage" or message.content.split(" ")[1].lower() == "dmg":
        with open("player.txt") as f:
          lines = f.readlines()
          f.close()
        kaki = []
        for line in lines:
          if len(line) > 1:
            kaki.append(tuple((line.split("&")[0], float(line.split("#")[4]))))
        tahat = sorted(kaki, key=lambda tahat: tahat[1])
        tahat.reverse()
        topDmg = ""
        i = 1
        for line in tahat:
          if i > 15:
            break
          topDmg += "Rank " + str(i) + " - " + await getName(line[0], client) + " (" + str(line[1]) + " Damage!)\n"
          i += 1
        topDmg = topDmg[:-1]
        myEmbed = discord.Embed(title="Top 15 Damage:", description=topDmg)
      elif message.content.split(" ")[1].lower() == "health" or message.content.split(" ")[1].lower() == "hp":
        with open("player.txt") as f:
          lines = f.readlines()
          f.close()
        kaki = []
        for line in lines:
          if len(line) > 1:
            kaki.append(tuple((line.split("&")[0], float(line.split("#")[3]))))
        tahat = sorted(kaki, key=lambda tahat: tahat[1])
        tahat.reverse()
        topHp = ""
        i = 1
        for line in tahat:
          if i > 15:
            break
          topHp += "Rank " + str(i) + " - " + await getName(line[0], client) + " (" + str(line[1]) + " Health!)\n"
          i += 1
        topHp = topHp[:-1]
        myEmbed = discord.Embed(title="Top 15 Health:", description=topHp)
      elif message.content.split(" ")[1].lower() == "petdamage" or message.content.split(" ")[1].lower() == "petdmg":
        with open("pet.txt") as f:
          lines = f.readlines()
          f.close()
        kaki = []
        for line in lines:
          if len(line) > 1 and '#' in line:
            kaki.append(tuple((line.split("&")[0], float(line.split("#")[4]), line.split("&")[1].split("#")[0])))
        tahat = sorted(kaki, key=lambda tahat: tahat[1])
        tahat.reverse()
        topDmg = ""
        i = 1
        for line in tahat:
          if i > 15:
            break
          topDmg += "Rank " + str(i) + " - " + await getName(line[0], client) + "'s " + line[2] +" (" + str(line[1]) + " Damage!)\n"
          i += 1
        topDmg = topDmg[:-1]
        myEmbed = discord.Embed(title="Top 15 Pet Damage:", description=topDmg)
      elif message.content.split(" ")[1].lower() == "pethealth" or message.content.split(" ")[1].lower() == "pethp":
        with open("pet.txt") as f:
          lines = f.readlines()
          f.close()
        kaki = []
        for line in lines:
          if len(line) > 1 and '#' in line:
            kaki.append(tuple((line.split("&")[0], float(line.split("#")[3]), line.split("&")[1].split("#")[0])))
        tahat = sorted(kaki, key=lambda tahat: tahat[1])
        tahat.reverse()
        topHp = ""
        i = 1
        for line in tahat:
          if i > 15:
            break
          topHp += "Rank " + str(i) + " - " + await getName(line[0], client) + "'s " + line[2] +" (" + str(line[1]) + " Health!)\n"
          i += 1
        topHp = topHp[:-1]
        myEmbed = discord.Embed(title="Top 15 Pet Health:", description=topHp)
      else:
        if "pet" in message.content.split(" ")[1].lower():
          await message.channel.send("did you meant \"top petdmg\" or \"top pethp\" ?")
          return 1
        else:
          await message.channel.send("did you meant \"top dmg\" or \"top hp\" ?")
          return 1
      await message.channel.send(embed=myEmbed)
      return 1
    else:
      await message.channel.send("did u mean to use: top <option>")
      return check
  return check

async def rank(message, check, client):
  if message.content.lower()[:4] == "rank":
    if message.content.count(" ") == 1: #check player rank
      msg = message.content.lower().split(" ")
      if len(msg[1]) > 0:
        if msg[1][2:-1].isdigit() == False:
          await message.channel.send("you have to tag a user! (player @user)")
          return 1
        if message.guild.get_member(int(msg[1][2:-1])) is not None:
          id = msg[1][2:-1]
        else:
          await message.channel.send("this user isn't in the server!")
          return 1
        
    else:
      id = str(message.author.id)
    with open("database.txt") as f:
      lines = f.readlines()
      f.close()
    if id not in "".join(lines):
      await message.channel.send("this player dont have player profile...")
      return 1
    kaki = []
    for line in lines:
      if len(line) > 1:
        kaki.append(tuple((line.split("{")[0], int(line.split(":")[2].split(",")[0]))))
    tahat = sorted(kaki, key=lambda tahat: tahat[1])
    tahat.reverse()
    i = 1
    for line in tahat:
      if id in line:
        break
      i += 1
    rankMoney = i
    i = 1
    with open("player.txt") as f:
      lines = f.readlines()
      f.close()
    kaki = []
    for line in lines:
      if len(line) > 1:
        kaki.append(tuple((line.split("&")[0], float(line.split("#")[4]))))
    tahat = sorted(kaki, key=lambda tahat: tahat[1])
    tahat.reverse()
    topDmg = ""
    i = 1
    for line in tahat:
      if id in line:
        break
      i += 1
    rankDmg = i
    i = 1
    with open("player.txt") as f:
      lines = f.readlines()
      f.close()
    kaki = []
    for line in lines:
      if len(line) > 1:
        kaki.append(tuple((line.split("&")[0], float(line.split("#")[3]))))
    tahat = sorted(kaki, key=lambda tahat: tahat[1])
    tahat.reverse()
    topDmg = ""
    i = 1
    for line in tahat:
      if id in line:
        break
      i += 1
    rankHp = i
    i = 1
    with open("pet.txt") as f:
      lines = f.readlines()
      f.close()
    kaki = []
    for line in lines:
      if len(line) > 1 and '#' in line:
        kaki.append(tuple((line.split("&")[0], float(line.split("#")[4]), line.split("&")[1].split("#")[0])))
    tahat = sorted(kaki, key=lambda tahat: tahat[1])
    tahat.reverse()
    topDmg = ""
    i = 1
    for line in tahat:
      if id in line:
        break
      i += 1
    rankPetDmg = i
    i = 1
    with open("pet.txt") as f:
      lines = f.readlines()
      f.close()
    kaki = []
    for line in lines:
      if len(line) > 1 and '#' in line:
        kaki.append(tuple((line.split("&")[0], float(line.split("#")[3]), line.split("&")[1].split("#")[0])))
    tahat = sorted(kaki, key=lambda tahat: tahat[1])
    tahat.reverse()
    topDmg = ""
    i = 1
    for line in tahat:
      if id in line:
        break
      i += 1
    rankPetHp = i
    i = 1

    myEmbed = discord.Embed(title=await getName(id, client) + "'s Ranks'")
    myEmbed.add_field(name="Money rank:", value="Ranked: " + str(rankMoney), inline=False)
    myEmbed.add_field(name="Damage rank:", value="Ranked: " + str(rankDmg), inline=False)
    myEmbed.add_field(name="Health rank:", value="Ranked: " + str(rankHp), inline=False)
    myEmbed.add_field(name="Pet Damage rank:", value="Ranked: " + str(rankPetDmg), inline=False)
    myEmbed.add_field(name="Pet Health rank:", value="Ranked: " + str(rankPetHp), inline=False)
    await message.channel.send(embed=myEmbed)
    return 1
  return check