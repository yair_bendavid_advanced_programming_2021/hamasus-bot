import discord
#bad words function, return true if the user message is in the bad words list.
async def bad_words(message):
  if 'קקי' in message.content or 'בולבול' in message.content or 'כושים' in message.content or 'שמן' in message.content or 'טוסיק' in message.content:
    await message.channel.send("למה לדבר ככה מלוכלך ילד")

#gay names list, same as bad_words but just for gay names.

async def gay_names(message):
  if 'רוי' == message.content:
    myEmbed = discord.Embed(title="לרוי הטסטר:", description="וואלה רוי אחלה טסטר מעריך")
    myEmbed.set_image(url="https://i.imgur.com/iNcQYHR.gif")
    await message.channel.send(embed=myEmbed)
  #add here more names. (using elif).

async def hamasus(message):
  if "hamasus" in message.content.lower() or "חמסוס" in message.content.lower() or "חמסאס" in message.content.lower():
    await message.channel.send(file=discord.File("Hamasus.mp4"))

async def say(message, check): 
  if message.content.lower()[:3] == "say":
    if True == True: 
      if message.content.count(" ") >= 2:
        msg = message.content.lower().split(" ", 2)
        if len(msg[1]) > 0 and len(msg[2]) > 0:
          if msg[1][2:-1].isdigit() == False:
            await message.channel.send("you have to tag a channel! (say #channel <message>)")
            return 1
          if message.guild.get_channel(int(msg[1][2:-1])) is not None:
            channel = message.guild.get_channel(int(msg[1][2:-1]))
            await channel.send(msg[2])
            return 1
          else:
            await message.channel.send("Thats channel is not exist!")
            return 1
        else:
          await message.channel.send("you should tag a channel and write your message!")
          return 1
      elif message.content.count(" ") == 1:
        await message.channel.send(message.content.split(" ")[1])
        return 1
      else:
        await message.channel.send("this command got 2 argumensts! (say #channel [msg] / say [msg])")
        return 1
    else:
      await message.channel.send("You dont have permission to use this command!")
      return 1
  else:
    return check