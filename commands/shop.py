import discord

async def shop(message, check):
  if message.content.lower() == 'shop':
    myEmbed = discord.Embed(title="Welcome to the shop!", description="Buy items to get stronger!, For now everything will appear in the inventory until i add database for pets & stats, pet stats and player stats will come soon as i got time for that.")
    text_file = open("shop.txt", "r")
    data = text_file.read()
    text_file.close()
    data = data.replace("-", " ")
    data = data.replace("$", " - $")
    myEmbed.add_field(name="Swords", value=data.split("#")[0], inline=False)
    myEmbed.add_field(name="Armours", value=data.split("#")[1], inline=False)
    myEmbed.add_field(name="Pets", value=data.split("#")[2], inline=False)
    myEmbed.add_field(name="Wings", value=data.split("#")[3], inline=False)
    myEmbed.add_field(name="Pet Food", value=data.split("#")[4], inline=False)
    myEmbed.add_field(name="Buildings", value=data.split("#")[5], inline=False)
    myEmbed.add_field(name="Accessories", value=data.split("#")[6], inline=False)
    await message.channel.send(embed=myEmbed)
    return 1
  return check