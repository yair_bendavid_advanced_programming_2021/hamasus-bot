import random
import discord
from datetime import datetime


#databse for cooldown
def check_exist_cd(message):
    #check if the user exist.
    text_file = open("cooldown.txt", "r")
    data = text_file.read()
    text_file.close()
    #if the user exist the update his data.
    if str(message.author.id) not in data:
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        #if the user not exist then add him to the database.
        with open("cooldown.txt", "a") as f:
            f.write(str(message.author.id) + " " + dt_string + "\n")
            f.close()
            return True
    else:
        return False


async def check_if_passed(message):
  #check if user has cd
  flag = check_exist_cd(message)
  if flag != True:
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    with open("cooldown.txt") as f:
      lines = f.readlines()
      f.close()
    for i in range(len(lines)):
      if str(message.author.id) in lines[i]:
        datetime_str = lines[i].split(" ")[1] + " " + lines[i].split(" ")[2].rstrip()
        datetime_object = datetime.strptime(datetime_str, "%d/%m/%Y %H:%M:%S")
        datetime_now = datetime.strptime(dt_string, "%d/%m/%Y %H:%M:%S")
        diff = datetime_now - datetime_object
        if diff.total_seconds() >= 86400: #the cd passed, need to return true and update curr date
          lines[i] = lines[i].split(" ")[0] + " " + dt_string + "\n"
          data = "".join(lines)
          with open("cooldown.txt", "w") as f:
            f.write(data)
            f.close()
            return True
        else: #the cooldown not passed.
          secondsLeft = 86400 - diff.total_seconds()
          hours = 0
          minutes = 0
          if int(secondsLeft / 60 / 60) > 0:
            hours = int(secondsLeft / 60 / 60)
            secondsLeft -= 3600 * hours
          if int(secondsLeft / 60) > 0:
            minutes = int(secondsLeft / 60)
            secondsLeft -= 60 * minutes
          await message.channel.send("You have to wait: " + str(hours) + " Hours, " + str(minutes) + " Minutes and " + str(int(secondsLeft)) + " seconds before u can use it again!")
          return False
  else:
    return True


#this function check if the user (by id) is in the database, if not then it add him and return false, if yes then it return true.
def check_exist(message):
    #check if the user exist.
    text_file = open("database.txt", "r")
    data = text_file.read()
    text_file.close()
    #if the user exist the update his data.
    if str(message.author.id) not in data:
        #if the user not exist then add him to the database.
        with open("database.txt", "a") as f:
            f.write(str(message.author.id) + "{exp:0,money:1000000,level:0}\n")
            f.close()


#every user can use this command once a day, it gives him money.
async def daily(message, check):
    if message.content.lower() == "daily" or message.content == "יומי":

        #make sure user in database.
        check_exist(message)
        flag = await check_if_passed(message)
        if flag == True:
            n = random.randint(0, 500)
            with open("database.txt") as f:
                lines = f.readlines()
                f.close()
            for i in range(len(lines)):
                if str(message.author.id) in lines[i]:
                    currMoney = lines[i].split(":")[2].split(",")[0]
                    lines[i] = lines[i].split(",")[0] + ",money:" + str(
                        n + int(currMoney)) + "," + lines[i].split(",")[2]
            data = "".join(lines)
            with open("database.txt", "w") as f:
                f.write(data)
                f.close()
            await message.channel.send(message.author.name + " got " + str(n) +
                                       "$")
        return 1
    return check


#this command return the amount of money the user have.
async def money(message, check):
    if message.content.lower()[:5] == "coins" or message.content.lower(
    )[:7] == "balance" or message.content.lower(
    )[:
      5] == "money" or message.content[:
                                       3] == "כסף" or message.content[:
                                                                      6] == "מטבעות":
        if message.content.count(" ") == 1:
            msg = message.content.lower().split(" ")
            if len(msg[1]) > 0:
                if msg[1][2:-1].isdigit() == False:
                    await message.channel.send("you have to tag a user! (" +
                                               msg[0] + " @user)")
                    return 1
                if message.guild.get_member(int(msg[1][2:-1])) is not None:
                    check_exist_id(msg[1][2:-1])
                    id = msg[1][2:-1]
                else:
                    await message.channel.send("this user isn't in the server!"
                                               )
                    return 1
        else:
            check_exist(message)
            id = str(message.author.id)
        with open("database.txt") as f:
            lines = f.readlines()
            f.close()
        for line in lines:
            if id in line:
                money = line.split(":")[2].split(",")[0]
        await message.channel.send(
            message.guild.get_member(int(id)).name + " has " + money + "$")
        return 1
    return check


#this command will add exp to member and lvl him up.
async def getExp(message):
    check_exist(message)
    with open("database.txt") as f:
        lines = f.readlines()
        f.close()
    for i in range(len(lines)):
        if str(message.author.id) in lines[i]:
            currExp = lines[i].split(":")[1].split(",")[0]
            currLevel = lines[i].split(":")[3].split("}")[0]
            #case level up.
            if (int(currLevel) + 1) * 5 == (int(currExp) + 1):
                currLevel = str(int(currLevel) + 1)
                lines[i] = lines[i].split(":")[0] + ":0,money:" + lines[
                    i].split(":")[2] + ":" + currLevel + "}\n"
                await message.channel.send(message.author.name +
                                           " just leveled up to level: " +
                                           currLevel)
            #not level up, then just add 1 exp.
            else:
                lines[i] = lines[i].split(":")[0] + ":" + str(
                    int(currExp) + 1) + "," + lines[i].split(
                        ",")[1] + "," + lines[i].split(",")[2]
    data = "".join(lines)
    with open("database.txt", "w") as f:
        f.write(data)
        f.close()


async def checkExp(message, check):
    if message.content.lower()[:3] == "exp":
        if message.content.count(" ") == 1:
            msg = message.content.lower().split(" ")
            if len(msg[1]) > 0:
                if msg[1][2:-1].isdigit() == False:
                    await message.channel.send(
                        "you have to tag a user! (exp @user)")
                    return 1
                if message.guild.get_member(int(msg[1][2:-1])) is not None:
                    check_exist_id(msg[1][2:-1])
                    id = msg[1][2:-1]
                else:
                    await message.channel.send("this user isn't in the server!"
                                               )
                    return 1
        else:
            check_exist(message)
            id = str(message.author.id)
        with open("database.txt") as f:
            lines = f.readlines()
            f.close()
        for line in lines:
            if id in line:
                currExp = line.split(":")[1].split(",")[0]
                currExp = str(int(currExp) + 1)
                currLevel = line.split(":")[3].split("}")[0]
        await message.channel.send(
            message.guild.get_member(int(id)).name + " has " + currExp + "/" +
            str(5 * (int(currLevel) + 1)) + " Exp!")
        return 1
    return check


async def checkLevel(message, check):
    if message.content.lower()[:5] == "level":
        if message.content.count(" ") == 1:
            msg = message.content.lower().split(" ")
            if len(msg[1]) > 0:
                if msg[1][2:-1].isdigit() == False:
                    await message.channel.send(
                        "you have to tag a user! (level @user)")
                    return 1
                if message.guild.get_member(int(msg[1][2:-1])) is not None:
                    check_exist_id(msg[1][2:-1])
                    id = msg[1][2:-1]
                else:
                    await message.channel.send("this user isn't in the server!"
                                               )
                    return 1
        else:
            check_exist(message)
            id = str(message.author.id)
        with open("database.txt") as f:
            lines = f.readlines()
            f.close()
        for line in lines:
            if id in line:
                currLevel = line.split(":")[3].split("}")[0]
        await message.channel.send(
            message.guild.get_member(int(id)).name + " level is: " +
            currLevel + "!")
        return 1
    return check


async def profile(message, check):
    if message.content.lower()[:7] == "profile" or message.content.lower(
    )[:6] == "פרופיל":
        if message.content.count(" ") == 1:
            msg = message.content.lower().split(" ")
            if len(msg[1]) > 0:
                if msg[1][2:-1].isdigit() == False:
                    await message.channel.send(
                        "you have to tag a user! (profile @user)")
                    return 1
                if message.guild.get_member(int(msg[1][2:-1])) is not None:
                    check_exist_id(msg[1][2:-1])
                    id = msg[1][2:-1]
                else:
                    await message.channel.send("this user isn't in the server!"
                                               )
                    return 1
        else:
            check_exist(message)
            id = str(message.author.id)
        with open("database.txt") as f:
            lines = f.readlines()
            f.close()
        for line in lines:
            if id in line:
                currExp = line.split(":")[1].split(",")[0]
                currExp = str(int(currExp) + 1)
                currLevel = line.split(":")[3].split("}")[0]
                myEmbed = discord.Embed(
                    title=message.guild.get_member(int(id)).name +
                    "'s profile",
                    description=
                    "this is your server profile, to see your player game profile type player in the chat"
                )
                myEmbed.set_thumbnail(
                    url=message.guild.get_member(int(id)).avatar_url)
                myEmbed.add_field(name="Stats:",
                                  value="\nExp: " + currExp + "/" +
                                  str(5 * (int(currLevel) + 1)) +
                                  "\n\nLevel: " + currLevel,
                                  inline=False)
                await message.channel.send(embed=myEmbed)
                return 1
    return check


async def userMoney(message):
    #make sure user in database.
    check_exist(message)
    with open("database.txt") as f:
        lines = f.readlines()
        f.close()
    for line in lines:
        if str(message.author.id) in line:
            money = line.split(":")[2].split(",")[0]
    return money


async def addMoney(id, amount):
    #make sure user in database.
    check_exist_id(id)
    with open("database.txt") as f:
        lines = f.readlines()
        f.close()
    for i in range(len(lines)):
        if id in lines[i]:
            currMoney = lines[i].split(":")[2].split(",")[0]
            lines[i] = lines[i].split(",")[0] + ",money:" + str(
                amount + int(currMoney)) + "," + lines[i].split(",")[2]
            data = "".join(lines)
            with open("database.txt", "w") as f:
                f.write(data)
                f.close()


async def removeMoney(id, amount):
    #make sure user in database.
    check_exist_id(id)
    with open("database.txt") as f:
        lines = f.readlines()
        f.close()
    for i in range(len(lines)):
        if id in lines[i]:
            currMoney = lines[i].split(":")[2].split(",")[0]
            lines[i] = lines[i].split(",")[0] + ",money:" + str(
                int(currMoney) - amount) + "," + lines[i].split(",")[2]
            data = "".join(lines)
            with open("database.txt", "w") as f:
                f.write(data)
                f.close()


async def gamble(message, check):
    if message.content.lower()[:6] == "gamble":
        if message.content.lower()[7:].isdigit():
            money = await userMoney(message)
            if int(money) >= int(
                    message.content.lower()[7:]) and int(money) > 0:
                n = random.randint(1, 10)
                if n % 2 == 0:
                    await addMoney(str(message.author.id),
                                   int(message.content.lower()[7:]))
                    await message.channel.send("congratulations!, you won " +
                                               message.content.lower()[7:] +
                                               " money!")
                else:
                    await removeMoney(str(message.author.id),
                                      int(message.content.lower()[7:]))
                    await message.channel.send("sorry, but you lost " +
                                               message.content.lower()[7:] +
                                               " money...")
            else:
                await message.channel.send("you dont have enough money!")
        else:
            await message.channel.send("invalid input!")
        return 1
    else:
        return check


def check_exist_id(id):
    #check if the user exist.
    text_file = open("database.txt", "r")
    data = text_file.read()
    text_file.close()
    #if the user exist the update his data.
    if id not in data:
        #if the user not exist then add him to the database.
        with open("database.txt", "a") as f:
            f.write(id + "{exp:0,money:0,level:0}\n")
            f.close()


async def give(message, check):
    if message.content.lower()[:4] == "give":
        if len(message.content.lower()[5:]) > 0 and message.content.count(
                " ") == 2:
            msg = message.content.lower().split(" ")
            if len(msg[1]) > 0 and len(msg[2]) > 0:
                if msg[2].isdigit():
                    #check if the user who send the money is in the db.
                    check_exist(message)
                    #check if the user we send money is in the db.
                    if msg[1][2:-1].isdigit() == False:
                        await message.channel.send(
                            "you have to tag a user! (give @user <amount>)")
                        return 1
                    if message.guild.get_member(int(msg[1][2:-1])) is not None:
                        check_exist_id(msg[1][2:-1])
                    else:
                        await message.channel.send(
                            "this user isn't in the server!")
                        return 1
                    money = await userMoney(message)
                    if int(money) >= int(msg[2]):
                        await addMoney(msg[1][2:-1], int(msg[2]))
                        await removeMoney(str(message.author.id), int(msg[2]))
                        await message.channel.send(msg[1] + ", " +
                                                   message.author.name +
                                                   " just sent you " + msg[2] +
                                                   " money!")
                    else:
                        await message.channel.send(
                            "you dont have enough money!")
                else:
                    await message.channel.send("invalid input!")
                    return 1
            else:
                await message.channel.send("you should tag a user!")
                return 1
        else:
            await message.channel.send(
                "this command got 2 argumensts! (give @user <amount>)")
            return 1
    else:
        return check


async def add(message, check):
    if message.content.lower()[:3] == "add":
        if str(message.author.id) == "882246230405701643" or str(
                message.author.id) == "439108851380387841":
            if len(message.content.lower()[4:]) > 0 and message.content.count(
                    " ") == 2:
                msg = message.content.lower().split(" ")
                if len(msg[1]) > 0 and len(msg[2]) > 0:
                    if msg[2].isdigit():
                        #check if the user who send the money is in the db.
                        check_exist(message)
                        #check if the user we send money is in the db.
                        if msg[1][2:-1].isdigit() == False:
                            await message.channel.send(
                                "you have to tag a user! (add @user <amount>)")
                            return 1
                        if message.guild.get_member(int(
                                msg[1][2:-1])) is not None:
                            check_exist_id(msg[1][2:-1])
                        else:
                            await message.channel.send(
                                "this user isn't in the server!")
                            return 1
                        await addMoney(msg[1][2:-1], int(msg[2]))
                        await message.channel.send(msg[1] + ", The admin " +
                                                   message.author.name +
                                                   " added you " + msg[2] +
                                                   " money!")
                    else:
                        await message.channel.send("invalid input!")
                        return 1
                else:
                    await message.channel.send("you should tag a user!")
                    return 1
            else:
                await message.channel.send(
                    "this command got 2 argumensts! (add @user <amount>)")
                return 1
        else:
            await message.channel.send(
                "You dont have permissions to use this command!")
            return 1
    else:
        return check


def hasMoney(id, amount):
    #make sure user in database.
    check_exist_id(id)
    with open("database.txt") as f:
        lines = f.readlines()
        f.close()
    for line in lines:
        if id in line:
            money = line.split(":")[2].split(",")[0]
    if int(money) >= amount:
        return True
    return False
