import discord

async def avatar(message, check):
  if message.content.lower()[:6] == "avatar":
    if message.content.count(" ") == 1:
      msg = message.content.lower().split(" ")
      if len(msg[1]) > 0:
        if msg[1][2:-1].isdigit() == False:
          await message.channel.send("you have to tag a user! (player @user)")
          return 1
        if message.guild.get_member(int(msg[1][2:-1])) is not None:
          id = msg[1][2:-1]
        else:
          await message.channel.send("this user isn't in the server!")
          return 1
    else:
      id = str(message.author.id)
    myEmbed = discord.Embed(title=message.guild.get_member(int(id)).name + "'s avatar")
    myEmbed.set_image(url=message.guild.get_member(int(id)).avatar_url)
    await message.channel.send(embed=myEmbed)
    return 1
  return check