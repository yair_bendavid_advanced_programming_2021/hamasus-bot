import discord

def check_exist_pet(id):
  #check if the user exist.
  text_file = open("pet.txt", "r")
  data = text_file.read()
  text_file.close()
  #if the user exist the update his data.
  if id not in data:
    #if the user not exist then add him to the database.
    with open("pet.txt", "a") as f:
      f.write(id + "&\n")
      f.close()

def gotPet(id):
  check_exist_pet(id)
  text_file = open("pet.txt", "r")
  lines = text_file.readlines()
  text_file.close()
  for line in lines:
    if id in line:
      if line.split("&")[1] == "" or line.split("&")[1] == "\n":
        return True
      return False

async def pet(message, check):
  if message.content.lower()[:3] == "pet":
    if message.content.count(" ") == 1:
      msg = message.content.lower().split(" ")
      if len(msg[1]) > 0:
        if msg[1][2:-1].isdigit() == False:
          await message.channel.send("you have to tag a user! (pet @user)")
          return 1
        if message.guild.get_member(int(msg[1][2:-1])) is not None:
          id = msg[1][2:-1]
        else:
          await message.channel.send("this user isn't in the server!")
          return 1
    else:
      id = str(message.author.id)
    flag = gotPet(id)
    if flag == False:
      with open("pet.txt") as f:
        lines = f.readlines()
        f.close()
      for i in range(len(lines)):
        if id in lines[i]:
          lines[i] = lines[i].replace("!", " - ")
          myEmbed = discord.Embed(title=message.guild.get_member(int(id)).name + "'s " + lines[i].split("&")[1].split("#")[0], description="feed your pet to help it level up! (with max lvl (200) your pet damage and health will multiply itself by 3!")
          if int(lines[i].split("#")[1]) == 200:
            myEmbed.add_field(name="Level: ", value="Max Level (200)", inline=False)
            myEmbed.add_field(name="Exp: ", value="MAX/MAX", inline=False)
          else: 
            myEmbed.add_field(name="Level: ", value=lines[i].split("#")[1], inline=False)
            myEmbed.add_field(name="Exp: ", value=lines[i].split("#")[2] + "/" + str(5 * (int(lines[i].split("#")[1]) + 1)), inline=False)
          myEmbed.add_field(name="Health: ", value=lines[i].split("#")[3], inline=False)
          myEmbed.add_field(name="Damage: ", value=lines[i].split("#")[4], inline=False)
          await message.channel.send(embed=myEmbed)
    else:
      await message.channel.send("You have to buy a pet to use this command!")
    return 1
  return check

async def abandon(message, check):
  if message.content.lower() == "abandon":
    flag = gotPet(str(message.author.id))
    if flag == False:
      check_exist_pet(str(message.author.id))
      with open("pet.txt") as f:
        lines = f.readlines()
        f.close()
      for i in range(len(lines)):
        if str(message.author.id) in lines[i]:
          await message.channel.send(message.author.name + " abandoned his " + lines[i].split("&")[1].split("#")[0] + "!")
          lines[i] = lines[i].split("&")[0] + "&\n"
          data = "".join(lines)
          with open("pet.txt", "w") as f:
            f.write(data)
            f.close()
          return 1
    else:
      await message.channel.send("You have to buy a pet to use this command!")
      return 1
    return 1
  return check

def petStats(pet):
  if pet.lower() == "bee":
    return "Bee#0#0#10#5\n"
  elif pet.lower() == "rabbit":
    return "Rabbit#0#0#20#10\n"
  elif pet.lower() == "frog":
    return "Frog#0#0#30#15\n"
  elif pet.lower() == "dog":
    return "Dog#0#0#40#20\n"
  elif pet.lower() == "cat":
    return "Cat#0#0#40#20\n"
  elif pet.lower() == "fox":
    return "Fox#0#0#70#35\n"
  elif pet.lower() == "wolf":
    return "Wolf#0#0#100#50\n"
  elif pet.lower() == "bear":
    return "Bear#0#0#150#75\n"
  elif pet.lower() == "tiger":
    return "Tiger#0#0#200#100\n"
  elif pet.lower() == "lion":
    return "Rabbit#0#0#500#250\n"
  elif pet.lower() == "shark":
    return "shark#0#0#1000#500\n"
  elif pet.lower() == "dragon":
    return "Rabbit#0#0#1500#750\n"
  elif pet.lower() == "demon":
    return "Rabbit#0#0#2500#1250\n"
  elif pet.lower() == "demon lord":
    return "Demon Lord#0#0#20000#10000\n"