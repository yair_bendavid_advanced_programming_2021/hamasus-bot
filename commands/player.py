import discord
from pet import gotPet
from userData import check_exist_id

def check_exist_player(id):
  #check if the user exist.
  text_file = open("player.txt", "r")
  data = text_file.read()
  text_file.close()
  #if the user exist the update his data.
  if id not in data:
    #if the user not exist then add him to the database.
    with open("player.txt", "a") as f:
      f.write(id + "&New Player#0#0#100#10\n") #make sure players dont call themself with # or &
      f.close()

def guildName(name):
  with open("guilds.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if name in lines[i].lower():
      return lines[i].split("&")[0]

def inGuild(name):
  with open("guilds.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if name in lines[i]:
      return True
  return False

def alreadyExistId(id):
  with open("player.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      return True
  return False

async def player(message, check):
  if message.content.lower()[:6] == "player":
    if len(message.content.lower()[7:]) > 0:
      msg = message.content.lower().split(" ")
      if msg[1][2:-1].isdigit() == False:
        await message.channel.send("you have to tag a user! (player @user)!")
        return 1
      if message.guild.get_member(int(msg[1][2:-1])) is not None or alreadyExistId(msg[1][2:-1]) == True:
        check_exist_player(msg[1][2:-1])
        check_exist_id(msg[1][2:-1])
        id = msg[1][2:-1]
      else:
        await message.channel.send("this user isn't in the server / dont exist!")
        return 1
    else:
      id = str(message.author.id)
      check_exist_player(str(message.author.id))
    with open("player.txt") as f:
        lines = f.readlines()
        f.close()
    for i in range(len(lines)):
      if id in lines[i]:
        myEmbed = discord.Embed(title=lines[i].split("&")[1].split("#")[0] + "'s profile", description="buy changeName or use your own to change your player name!, feed yourself to gain exp and get stronger and buy items from the shop to get more health and damage!")
        flag = inGuild(lines[i].split("&")[1].split("#")[0])
        if flag == True:
          guild = guildName(lines[i].split("&")[1].split("#")[0])
          myEmbed.add_field(name="Guild: ", value=guild, inline=False)
        if int(lines[i].split("#")[1]) == 200:
          myEmbed.add_field(name="Level: ", value="Max Level (200)", inline=False)
          myEmbed.add_field(name="Exp: ", value="MAX/MAX", inline=False)
        else: 
          myEmbed.add_field(name="Level: ", value=lines[i].split("#")[1], inline=False)
          myEmbed.add_field(name="Exp: ", value=lines[i].split("#")[2] + "/" + str(5 * (int(lines[i].split("#")[1]) + 1)), inline=False)
        myEmbed.add_field(name="Health: ", value=lines[i].split("#")[3], inline=False)
        myEmbed.add_field(name="Damage: ", value=lines[i].split("#")[4], inline=False)
        with open("database.txt") as f:
            lines = f.readlines()
            f.close()
        for line in lines:
            if id in line:
                money = line.split(":")[2].split(",")[0]
        myEmbed.add_field(name="Money: ", value=money, inline=False)
        flag = gotPet(id)
        if flag == True:
          myEmbed.add_field(name="Pet", value="You Dont have a pet yet, buy one in the shop", inline=False)
        else:
          with open("pet.txt") as f:
            lines = f.readlines()
            f.close()
          for i in range(len(lines)):
            if id in lines[i]:
              lines[i] = lines[i].replace("!", " - ")
              myEmbed.add_field(name="Pet: " + lines[i].split("&")[1].split("#")[0], value="raise your pet stats by feeding it!", inline=False)
              if int(lines[i].split("#")[1]) == 200:
                myEmbed.add_field(name="Level: ", value="Max Level (200)", inline=False)
                myEmbed.add_field(name="Exp: ", value="MAX/MAX", inline=False)
              else: 
                myEmbed.add_field(name="Level: ", value=lines[i].split("#")[1], inline=False)
                myEmbed.add_field(name="Exp: ", value=lines[i].split("#")[2] + "/" + str(5 * (int(lines[i].split("#")[1]) + 1)), inline=False)
              myEmbed.add_field(name="Health: ", value=lines[i].split("#")[3], inline=False)
              myEmbed.add_field(name="Damage: ", value=lines[i].split("#")[4], inline=False)
        await message.channel.send(embed=myEmbed)
        return 1
  return check

def gotChangeName(id):
  with open("inventory.txt") as f:
      lines = f.readlines()
      f.close()
      for i in range(len(lines)):
        if id in lines[i]:
          if "changename" in lines[i].lower():
            return True
          return False

def useChangeName(id, item):
    foodName = item
    with open("inventory.txt") as f:
      lines = f.readlines()
      f.close()
      for i in range(len(lines)):
        if id in lines[i]:
          if foodName.lower() in lines[i].lower():
            if '!' in lines[i].lower().split(foodName.lower())[1].split("#")[0]:
              if int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[1]) > 2:
                if '#' in lines[i].lower().split(foodName.lower())[1]:
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[0] + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("#")[0].split("!")[1]) - 1)) + '#' + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
                elif '#' in lines[i]:
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("!")[1]) - 1))
                else:
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + '!' + str(int(int(lines[i].lower().split(foodName.lower())[1].split("!")[1]) - 1))
              else: #got 2 of this item so remove '!'
                if '#' in lines[i].lower().split(foodName.lower())[1]:
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower() + '#' + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
                elif '#' in lines[i]: #above 2 items in inventory
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower()
                else: #only this item in inventory
                  lines[i] = lines[i].lower().split(foodName.lower())[0] + foodName.lower()
            else:
              if '#' in lines[i].lower().split(foodName.lower())[1]:
                lines[i] = lines[i].lower().split(foodName.lower())[0] + lines[i].lower().split(foodName.lower())[1].split("#", 1)[1]
              elif '#' in lines[i]:
                lines[i] = lines[i].replace("\n", "")
                lines[i] = lines[i].lower().split('#' + foodName.lower())[0] + "\n"
              else:
                lines[i] = lines[i].replace("\n", "")
                lines[i] = lines[i].lower().split(foodName.lower())[0] + "\n"
            lines[i] = lines[i].replace("\n", "")
            lines[i] = lines[i] + "\n"
            data = "".join(lines)
            with open("inventory.txt", "w") as f:
              f.write(data)
              f.close()
            with open("player.txt") as f:
              lines = f.readlines()
              f.close()
            for i in range(len(lines)):
              if id in lines[i]:
                oldName = lines[i].lower().split("&")[1].split("#")[0]
                lines[i] = lines[i].lower().split(oldName.lower())[0] + "New Player" + lines[i].lower().split(oldName.lower())[1]
                data = "".join(lines)
                with open("player.txt", "w") as f:
                  f.write(data)
                  f.close()

def alreadyExistName(name):
  with open("player.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if name in lines[i]:
      return True
  return False

def getOldName(id):
  with open("player.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      return lines[i].lower().split("&")[1].split("#")[0]

def changeNameInGuild(name, oldName):
  with open("guilds.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if oldName in lines[i]:
      lines[i] = lines[i].replace(oldName, name)
      data = "".join(lines)
      with open("guilds.txt", "w") as f:
        f.write(data)
        f.close()

async def changeName(message, check):
  if message.content.lower()[:10] == "changename":
    flag1 = alreadyExistName(message.content.lower()[11:])
    if flag1 == False:
      if '#' in message.content.lower()[11:] or '&' in message.content.lower()[11:]:
        await message.channel.send("you cant put # and & in your name")
        return 1
      elif len(message.content.lower()[11:]) > 20:
        await message.channel.send("max name len 20!")
        return 1
      else:
        check_exist_player(str(message.author.id))
        with open("player.txt") as f:
            lines = f.readlines()
            f.close()
        for i in range(len(lines)):
          if str(message.author.id) in lines[i]:
            if "new player" not in lines[i].lower():
              if gotChangeName(str(message.author.id)) == True:
                oldName = getOldName(str(message.author.id))
                changeNameInGuild(message.content[11:], oldName)
                useChangeName(str(message.author.id), "changename")
                with open("player.txt") as f:
                  lines = f.readlines()
                  f.close()
              else:
                await message.channel.send("you have to buy ChangeName from the shop first!")
                return 1
            if lines[i].lower().split("&")[1].split("#")[0] == "new player":
              lines[i] = lines[i].lower().split("new player")[0] + message.content[11:] + lines[i].lower().split("new player")[1]
              data = "".join(lines)
              with open("player.txt", "w") as f:
                f.write(data)
                f.close()
              await message.channel.send("you have successfuly changed your name to " + message.content[11:] + "!")
              return 1
            else:
              await message.channel.send("you have to buy ChangeName from the shop first!")
              return 1
    else:
      await message.channel.send("There is already player with that name!")
      return 1
  return check