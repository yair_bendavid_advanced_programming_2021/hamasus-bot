import discord
#remember, when i do equip command than do this:
#totalDamage = originalDamage + alreadyEquipedItemsDamage + currEquipedItemDamage.
#totalHealth = originalHealth + alreadyEquipedItemsHealth + currEquipedItemHealth.
#float(totalDamage) + float(totalDamage / 10) * float(level)
#and also remember to make sure that you equip only one thing of type, like you can equip only 1 sword and only 1 wings and only 1 armour and stuff like that.

def check_exist_item(id):
  #check if the user exist.
  text_file = open("items.txt", "r")
  data = text_file.read()
  text_file.close()
  #if the user exist the update his data.
  if id not in data:
    #if the user not exist then add him to the database.
    with open("items.txt", "a") as f:
      f.write(id + "&\n")
      f.close()

def buyItem(id, item):
  #make sure user in database.
  check_exist_item(id)
  with open("items.txt") as f:
    lines = f.readlines()
    f.close()
  if item.lower() == "wood sword":
    item = "wood sword%5"
  elif item.lower() == "stone sword":
    item = "stone sword%7"
  elif item.lower() == "iron sword":
    item = "iron sword%12"
  elif item.lower() == "diamond sword":
    item = "diamond sword%22"
  elif item.lower() == "epic sword":
    item = "epic sword%30"
  elif item.lower() == "dragon sword":
    item = "dragon sword%100"
  elif item.lower() == "ancient holysword":
    item = "ancient holysword%500"
  elif item.lower() == "infinite sword":
    item = "infinite sword%1000"
  elif item.lower() == "leather armour":
    item = "leather armour^10"
  elif item.lower() == "plate armour":
    item = "plate armour^50"
  elif item.lower() == "epic armour":
    item = "epic armour^200"
  elif item.lower() == "dragon armour":
    item = "dragon armour^500"
  elif item.lower() == "legendary armour":
    item = "legendary armour^1000"
  elif item.lower() == "angel wings":
    item = "angel wings%300^200"
  elif item.lower() == "dragon wings":
    item = "dragon wings%800^500"
  elif item.lower() == "evil wings":
    item = "evil wings%1000^800"
  elif item.lower() == "flame wings":
    item = "flame wings%1200^1000"
  elif item.lower() == "demon wings":
    item = "demon wings%2000^1500"
  for i in range(len(lines)):
    if id in lines[i]:
      if item + "!" in lines[i]: #means has above 1
        amount = int(lines[i].split("!")[1].split("#")[0])
        currItems = lines[i].split("&")[1].split(item + "!" + str(amount))[0] + item + "!" + str(amount + 1) + lines[i].split("&")[1].split(item + "!" + str(amount))[1]
      elif item in lines[i]: #means has only 1
        currItems = lines[i].split("&")[1].split(item)[0] + item + "!2" + lines[i].split("&")[1].split(item)[1]
      else: #dont have the item
        if len(lines[i].split("&")[1]) > 0 and lines[i].split("&")[1] != "\n":
          currItems = lines[i].split("&")[1] + "#" + item
        else:
          currItems = lines[i].split("&")[1] + item
      currItems = currItems.replace("\n", "")
      lines[i] = lines[i].split("&")[0] + "&" + currItems + "\n"
      data = "".join(lines)
      with open("items.txt", "w") as f:
        f.write(data)
        f.close()

async def items(message, check):
  if message.content.lower()[:5] == "items":
    if message.content.count(" ") == 1:
      msg = message.content.lower().split(" ")
      if len(msg[1]) > 0:
        if msg[1][2:-1].isdigit() == False:
          await message.channel.send("you have to tag a user! (exp @user)")
          return 1
        if message.guild.get_member(int(msg[1][2:-1])) is not None:
          check_exist_item(msg[1][2:-1])
          id = msg[1][2:-1]
        else:
          await message.channel.send("this user isn't in the server!")
          return 1
    else:
      check_exist_item(str(message.author.id))
      id = str(message.author.id)
    with open("items.txt") as f:
      lines = f.readlines()
      f.close()
    for i in range(len(lines)):
      if id in lines[i]:
        myEmbed = discord.Embed(title=message.guild.get_member(int(id)).name + "'s Items:")
        lines[i] = lines[i].replace("*", " (equiped)")
        lines[i] = lines[i].replace("\n", "")
        myItems = lines[i].split("&")[1].split("#")
        for i in range(len(myItems)):
          if len(lines[i].split("&")[1]) > 0 and ("%" in myItems[i] or "^" in myItems[i]): #means has items
            if "wings" in myItems[i].lower():
              if "!" in myItems[i]:
                myEmbed.add_field(name=myItems[i].split("%")[0], value="Health: " + myItems[i].split("%")[1].split("^")[0] + "\nDamage: " + myItems[i].split("^")[1].split("!")[0] + "\nAmount: " + myItems[i].split("!")[1], inline=False)
              else:
                myEmbed.add_field(name=myItems[i].split("%")[0], value="Health: " + myItems[i].split("%")[1].split("^")[0] + "\nDamage: " + myItems[i].split("^")[1])
            else:
              if "%" in myItems[i]:
                if "!" in myItems[i]:
                  myEmbed.add_field(name=myItems[i].split("%")[0], value="Damage: " + myItems[i].split("%")[1].split("!")[0] + "\nAmount: " + myItems[i].split("!")[1], inline=False)
                else:
                  myEmbed.add_field(name=myItems[i].split("%")[0], value="Damage: " + myItems[i].split("%")[1], inline=False)
              else:
                if "!" in myItems[i]:
                  myEmbed.add_field(name=myItems[i].split("^")[0], value="Health: " + myItems[i].split("^")[1].split("!")[0] + "\nAmount: " + myItems[i].split("!")[1], inline=False)
                else:
                  myEmbed.add_field(name=myItems[i].split("^")[0], value="Health: " + myItems[i].split("^")[1], inline=False)
        await message.channel.send(embed=myEmbed)
    return 1
  return check

#check if the user have this item or not.
def gotItem(id, item):
  with open("items.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      if item in lines[i].lower():
        return True
      return False

#check if the user already has item of this type equipped
def alreadyEquipped(id, item):
  with open("items.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      if item.split(" ")[1] + "*" in lines[i].lower():
        return True
      return False

#add * to the end of the item we equipping to mark it as equipped.
def markEquip(id, item):
  with open("items.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      lines[i] = lines[i].lower().split(item)[0] + item.lower() + '*' + lines[i].lower().split(item)[1]
      data = "".join(lines)
      with open("items.txt", "w") as f:
        f.write(data)
        f.close()

#this function add the items stats to the curr player stats!.
def addItemStats(id, item):
  with open("items.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      if "wings" in item:
        hp = float(lines[i].lower().split(item + "%")[1].split("^")[0])
        if '#' in lines[i].lower().split(item)[1]:
          if '!' in lines[i].lower().split(item)[1].split("#")[0]:
            dmg = float(lines[i].lower().split(item)[1].split("^")[1].split("!")[0])
          else:
            dmg = float(lines[i].lower().split(item)[1].split("^")[1].split("#")[0])
        else: #last or only item
          if '!' in lines[i].lower().split(item)[1]:
            dmg = float(lines[i].lower().split(item)[1].split("^")[1].split("!")[0])
          else:
            dmg = float(lines[i].lower().split(item)[1].split("^")[1])
        #add wings dmg and hp to player stats.
        with open("player.txt") as f:
          lines = f.readlines()
          f.close()
        for i in range(len(lines)):
          if id in lines[i]:
            totalHp = float(lines[i].split("#")[3]) + hp
            totalDmg = float(lines[i].split("#")[4]) + dmg
            lines[i] = lines[i].replace("\n", "")
            lines[i] = lines[i].split("#")[0] + "#" + lines[i].split("#")[1] + "#" + lines[i].split("#")[2] + "#" + str(totalHp) + "#" + str(totalDmg) + "\n"
            data = "".join(lines)
            with open("player.txt", "w") as f:
              f.write(data)
              f.close()
      elif "armour" in item:
        if '#' in lines[i].lower().split(item)[1]:
          if '!' in lines[i].lower().split(item)[1].split("#")[0]:
            hp = float(lines[i].lower().split(item)[1].split("^")[1].split("!")[0])
          else:
            hp = float(lines[i].lower().split(item)[1].split("^")[1].split("#")[0])
        else:
          if '!' in lines[i].lower().split(item)[1]:
            hp = float(lines[i].lower().split(item + "^")[1].split("!")[0])
          else:
            hp = float(lines[i].lower().split(item + "^")[1])
        #add armour hp to player stats
        with open("player.txt") as f:
          lines = f.readlines()
          f.close()
        for i in range(len(lines)):
          if id in lines[i]:
            totalHp = float(lines[i].split("#")[3]) + hp
            lines[i] = lines[i].replace("\n", "")
            lines[i] = lines[i].split("#")[0] + "#" + lines[i].split("#")[1] + "#" + lines[i].split("#")[2] + "#" + str(totalHp) + "#" + lines[i].split("#")[4] + "\n"
            data = "".join(lines)
            with open("player.txt", "w") as f:
              f.write(data)
              f.close()
            
      elif "sword" in item:
        if '#' in lines[i].lower().split(item)[1]:
          if '!' in lines[i].lower().split(item)[1].split("#")[0]:
            dmg = float(lines[i].lower().split(item)[1].split("%")[1].split("!")[0])
          else:
            dmg = float(lines[i].lower().split(item)[1].split("%")[1].split("#")[0])
        else:
          if '!' in lines[i].lower().split(item)[1]:
            dmg = float(lines[i].lower().split(item + "%")[1].split("!")[0])
          else:
            dmg = float(lines[i].lower().split(item + "%")[1])
        #add sword dmg to player stats
        with open("player.txt") as f:
          lines = f.readlines()
          f.close()
        for i in range(len(lines)):
          if id in lines[i]:
            totalDmg = float(lines[i].split("#")[4]) + dmg
            lines[i] = lines[i].replace("\n", "")
            lines[i] = lines[i].split("#")[0] + "#" + lines[i].split("#")[1] + "#" + lines[i].split("#")[2] + "#" + lines[i].split("#")[3] + "#" + str(totalDmg) + "\n"
            data = "".join(lines)
            with open("player.txt", "w") as f:
              f.write(data)
              f.close()

def defaultName(id):
  with open("player.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      if lines[i].lower().split("&")[1].split("#")[0] == "new player":
        return True
      return False
      

async def equip(message, check):
  if message.content.lower()[:5] == "equip":
    flag = defaultName(str(message.author.id))
    if flag == False:
      item = message.content.lower()[6:]
      flag1 = gotItem(str(message.author.id), item)
      if flag1 == True:
        flag2 = alreadyEquipped(str(message.author.id), item)
        if flag2 == False:
          addItemStats(str(message.author.id), item)
          markEquip(str(message.author.id), item)
          await message.channel.send(item + " has beed equipped!")
          return 1
        else:
          await message.channel.send("your'e already equip item of this type!, use unequip first!")
          return 1
      else:
        await message.channel.send("you dont have this item!")
        return 1
    else:
      await message.channel.send("Please change your player name first! (changeName <name>)")
  return check
#this function remove the items stats from the curr player stats!.
def removeItemStats(id, item):
  with open("items.txt") as f:
    lines = f.readlines()
    f.close()
  for i in range(len(lines)):
    if id in lines[i]:
      if "wings" in item:
        hp = float(lines[i].lower().split(item + "%")[1].split("^")[0])
        if '#' in lines[i].lower().split(item)[1]:
          if '!' in lines[i].lower().split(item)[1].split("#")[0]:
            dmg = float(lines[i].lower().split(item)[1].split("^")[1].split("!")[0])
          else:
            dmg = float(lines[i].lower().split(item)[1].split("^")[1].split("#")[0])
        else: #last or only item
          if '!' in lines[i].lower().split(item)[1]:
            dmg = float(lines[i].lower().split(item)[1].split("^")[1].split("!")[0])
          else:
            dmg = float(lines[i].lower().split(item)[1].split("^")[1])
        #add wings dmg and hp to player stats.
        with open("player.txt") as f:
          lines = f.readlines()
          f.close()
        for i in range(len(lines)):
          if id in lines[i]:
            totalHp = float(lines[i].split("#")[3]) - hp
            totalDmg = float(lines[i].split("#")[4]) - dmg
            totalHp = round(totalHp, 2)
            totalDmg = round(totalDmg, 2)
            lines[i] = lines[i].replace("\n", "")
            lines[i] = lines[i].split("#")[0] + "#" + lines[i].split("#")[1] + "#" + lines[i].split("#")[2] + "#" + str(totalHp) + "#" + str(totalDmg) + "\n"
            data = "".join(lines)
            with open("player.txt", "w") as f:
              f.write(data)
              f.close()
      elif "armour" in item:
        if '#' in lines[i].lower().split(item)[1]:
          if '!' in lines[i].lower().split(item)[1].split("#")[0]:
            hp = float(lines[i].lower().split(item)[1].split("^")[1].split("!")[0])
          else:
            hp = float(lines[i].lower().split(item)[1].split("^")[1].split("#")[0])
        else:
          if '!' in lines[i].lower().split(item)[1]:
            hp = float(lines[i].lower().split(item + "^")[1].split("!")[0])
          else:
            hp = float(lines[i].lower().split(item + "^")[1])
        #add armour hp to player stats
        with open("player.txt") as f:
          lines = f.readlines()
          f.close()
        for i in range(len(lines)):
          if id in lines[i]:
            totalHp = float(lines[i].split("#")[3]) - hp
            totalHp = round(totalHp, 2)
            lines[i] = lines[i].replace("\n", "")
            lines[i] = lines[i].split("#")[0] + "#" + lines[i].split("#")[1] + "#" + lines[i].split("#")[2] + "#" + str(totalHp) + "#" + lines[i].split("#")[4] + "\n"
            data = "".join(lines)
            with open("player.txt", "w") as f:
              f.write(data)
              f.close()
            
      elif "sword" in item:
        if '#' in lines[i].lower().split(item)[1]:
          if '!' in lines[i].lower().split(item)[1].split("#")[0]:
            dmg = float(lines[i].lower().split(item)[1].split("%")[1].split("!")[0])
          else:
            dmg = float(lines[i].lower().split(item)[1].split("%")[1].split("#")[0])
        else:
          if '!' in lines[i].lower().split(item)[1]:
            dmg = float(lines[i].lower().split(item + "%")[1].split("!")[0])
          else:
            dmg = float(lines[i].lower().split(item + "%")[1])
        #add sword dmg to player stats
        with open("player.txt") as f:
          lines = f.readlines()
          f.close()
        for i in range(len(lines)):
          if id in lines[i]:
            totalDmg = float(lines[i].split("#")[4]) - dmg
            totalDmg = round(totalDmg, 2)
            lines[i] = lines[i].replace("\n", "")
            lines[i] = lines[i].split("#")[0] + "#" + lines[i].split("#")[1] + "#" + lines[i].split("#")[2] + "#" + lines[i].split("#")[3] + "#" + str(totalDmg) + "\n"
            data = "".join(lines)
            with open("player.txt", "w") as f:
              f.write(data)
              f.close()
  
  
#unequip the item choosed
async def unequip(message, check):
  if message.content.lower()[:7] == "unequip":
    item = message.content.lower()[8:]
    flag1 = gotItem(str(message.author.id), item)
    if flag1 == True:
      flag2 = alreadyEquipped(str(message.author.id), item)
      if flag2 == True:
        with open("items.txt") as f:
          lines = f.readlines()
          f.close()
        for i in range(len(lines)):
          if str(message.author.id) in lines[i]:
            lines[i] = lines[i].lower().split(item + "*")[0] + item.lower() + lines[i].lower().split(item + "*")[1]
            data = "".join(lines)
            with open("items.txt", "w") as f:
              f.write(data)
              f.close()
            removeItemStats(str(message.author.id), item)
            await message.channel.send(item + " has been unequipped!")
            return 1
      else:
        await message.channel.send("this item isn't equipped!")
        return 1
    else:
      await message.channel.send("you dont have item with that name!")
      return 1
  return check