import sys
import discord
sys.path.insert(1, './commands')
#if you want to use a function from other file you got to do: from file.py import <func>
from functions import bad_words
from functions import gay_names
from functions import hamasus
from functions import say
from userData import daily
from userData import money
from userData import getExp
from userData import checkExp
from userData import checkLevel
from userData import profile
from userData import gamble
from userData import give
from userData import add
from invite import invite_link
from help import help
from credits import credits
from avatar import avatar
from shop import shop
from inventory import buy
from inventory import inventory
from pet import pet
from pet import abandon
from food import food
from food import feed
from update import update
from player import player
from player import changeName
from items import items
from items import equip
from items import unequip
from guilds import createGuild
from guilds import guilds
from guilds import join
from guilds import accept
from guilds import kick
from bj import blackJack
from bj import Hit
from bj import stand
from ranks import top
from ranks import rank
import os
import keep_alive

keep_alive.keep_alive()
my_secret = os.environ['TOKEN']

intents = discord.Intents.default()
intents.members = True

client = discord.Client(intents=intents)
#notify that the bot is ready (in the log)
@client.event
async def on_ready():
  # Setting `Watching ` status
  print("bot is ready!")

@client.event
async def on_member_join(member):
  myEmbed=discord.Embed(title=f"Hello {member.name} and welcome to Official HamasusBot server!",description=f"{member.mention}, Please read first <#963925682755350568>\nHave a good day!")
  myEmbed.set_thumbnail(url=member.avatar_url)
  if member.guild.name == "Official HamasusBot":
    channel = client.get_channel(963925648462741535)
    myEmbed.add_field(name="We are Currently: ",value=str(member.guild.member_count) + " members!",inline=False)
  elif member.guild.name == "HamasusBot":
    channel = client.get_channel(964094843477426196)
    myEmbed.add_field(name="We are Currently: ",value=str(member.guild.member_count) + " members!",inline=False)
  await channel.send(embed=myEmbed)

"""
basically the commands but here i use just call the functions becuase i want the main to be clean, you should create the command itself in other file, look comment 3 down.
"""
@client.event
async def on_message(message):
  for guild in client.guilds:
    if "Official HamasusBot" in guild.name:
      await client.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=str(len(client.guilds)) + " servers"))
  if message.author.bot:
    return
  #commands.
  check = 0
  await bad_words(message)
  await gay_names(message)
  await hamasus(message)
  check = await avatar(message, check)
  check = await help(message, check)
  check = await credits(message, check)
  check = await daily(message, check)
  check = await invite_link(message, check)
  check = await money(message, check)
  check = await checkExp(message, check)
  check = await checkLevel(message, check)
  check = await profile(message, check)
  check = await gamble(message, check)
  check = await give(message, check)
  check = await shop(message, check)
  check = await buy(message, check)
  check = await inventory(message, check)
  check = await pet(message, check)
  check = await abandon(message, check)
  check = await food(message, check)
  check = await feed(message, check)
  check = await update(message, check)
  check = await player(message, check)
  check = await changeName(message, check)
  check = await items(message, check)
  check = await equip(message,check)
  check = await unequip(message, check)
  check = await createGuild(message, check)
  check = await guilds(message, check)
  check = await join(message, check)
  check = await accept(message, check)
  check = await kick(message, check)
  check = await add(message, check)
  check = await say(message, check)
  check = await blackJack(message, check)
  check = await Hit(message, check)
  check = await stand(message, check)
  check = await top(message, check, client)
  check = await rank(message, check, client)
  #important! if player change name make sure the name in the guild also change. also make sure that player name cant be "new player" if he joins a guild!
  if check != 1:
    await getExp(message)
#this is the bot token, this code line is running the bot with that token.
client.run(my_secret)


"""
if you got any comment that you want to notify each other you should write it here in the protocol: comment <num>, name:
for example heres mine:

comment 1, yair: if you want to create a new command you should create a new .py file in the commands folder do there the function and import it there by the protocol: from FileName import FuncName.

comment 2, yair: the keep_alive file used to make sure that this site wont get closed with uptimer bot that i created, that why you can run here the bot and leave the site, and the boot will still working because this site is still on running in the uptimer but (i created this uptimer bot in uptimerobot.com, you shouldn't thouch any of them but if you want to see it ask me in whatsapp i will give you my account there).

comment 3, yair: the functions.py used for stuff thats not exactly commands, like the bad_words function, and the gay_names functions, which not really commands, commands should be which exactly one word to call it, like you can create command called avater that when someone type in the chat avatar it sends this user avatar, then the file of this command should be called avatar.py, and the only functions there has to be connected just for this function!, ofc put it in the commands folder, and when you want to import it do in the way i described in comment 1.

comment 4, yair:
everytime when you want the bot to send a message you should do await before, and to use await the function should be async function, which means it gonna look something like that:
async def example(message):
  if message.content == banana:
    await message.channel.send("niggers").
btw, when you doing command in english i higly recommend you to use lower() func on the message, this way even if the user message was like InViTe it will recognize it as invite (message.content.lower()).

comment 5, YourName:

"""

"""
to do list, stuff that need to fix / complete.

todo 1, yair:
need to change the commands with argument to be by split, this way the command name can be at any length and also support hebrew, and also change all the check_exist to be by id and no by message and do not deplicate check it.

todo 2, yair:
add checkGuildLeader(id) function that check if that player is leader of a guild, add capacity to guilds depend on the guild house (starts with the smallest house, 5 members, should probably add database to guilds house which contain the guildHouse name#MembersCapacity#bankCapacity#Members#BankMoney)

add totalDamage function that get the total Damage of player and his pet and totalHealth of the same thing but to their hp, and also totalDamageGuild and totalHealthGuild of all the guild members + their pets (for every member dmg call the totalDamage function and for hp the totalHealt and add them to su, var that contain all the members total hp and var for all the members total dmg) for the guild boss fights.

for hunt i should add more pets that doesn't appear in the shop and cant buy them, and also add pets fight that our pets fight other pets to earn money & exp like when i fight monsters, add it cd.

todo 3, yair:
add AvgItemPower to check the avg of the items the player wearing, and also add items for hands, head, legs.

todo 4, yair:
add upgrade to items (money for upgrade with success rate depend on the item level).

todo 5, yair:
add enchantment to items (5 items enchatment tiers, add a few more % to some of the item stats, depend on the enchantment tier).

todo 6, yair:
add stats for fame (pk, dungeounes (like finished 5 dungeons of tier III and for other tiers), Bosses (also by tier, like killed 5 Bosses of tier II), GuildWars Won)

todo 7, yair:
add Event items, Drop items (items can be earned only from dungeons and guild wars).

todo 8, yair:
add level requirment to items. (at some point maybe even stats requirment for item)

todo 9, yair:
add fame for weapons (every kill i do with the weapon give some exp to my mastery of that weapon, and while it levels add bonus power to it, with that someone with level 100 mystery will have like 50 more damage for example for that same weapon someone else use (same level and everything) at mastery 0)

"""